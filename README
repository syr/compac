In Android applications, third-party components may bring potential security problems,
because they have the same privilege as the applications but cannot be fully trusted.
It is desirable if their privileges can be restricted. To minimize the privilege of 
the third-party components, we develop Compac to achieve a fine-grained access control
at application's component level. Compac allows developers and users to assign a subset 
of an application's permissions to some of the application's components. By leveraging 
the runtime Java package information, the system can acquire the component information 
that is running in the application. After that, the system makes decisions on privileged 
access requests according to the policy denied by the developer and user. We have 
implemented the prototype in Android 4.0.4, and have conducted a comprehensive evaluation.
Our case studies show that Compac can effectively restrict the third-party components' 
permissions. Antutu benchmark shows that the overall score of our work achieves 97.4%, 
compared with the score of the original Android. In conclusion, Compac can mitigate the 
damage caused by third-party components with ignorable overhead.
