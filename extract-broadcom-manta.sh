#!/bin/bash
#
# (C) Broadcom Corporation
#
# Usage is subject to the enclosed license agreement

echo
echo The license for this software will now be displayed.
echo You must agree to this license before using this software.
echo
echo -n Press Enter to view the license
read dummy
echo

more << __EOF__
THIS DEVELOPER SOFTWARE LICENSE AGREEMENT (THE "AGREEMENT") IS A LEGALLY
BINDING AGREEMENT BETWEEN BROADCOM CORPORATION ("LICENSOR") AND
YOU OR THE LEGAL ENTITY YOU REPRESENT ("You" or its possessive, "Your"). BY
TYPING "I ACCEPT" WHERE INDICATED YOU ACKNOWLEDGE THAT YOU HAVE READ THIS
AGREEMENT, UNDERSTAND IT AND AGREE TO BE BOUND BY ITS TERMS AND CONDITIONS.
IF YOU DO NOT AGREE TO THESE TERMS YOU MUST DISCONTINUE THE INSTALLATION
PROCESS AND YOU SHALL NOT USE THE SOFTWARE OR RETAIN ANY COPIES OF THE
SOFTWARE OR DOCUMENTATION. ANY USE OR POSSESSION OF THE SOFTWARE BY YOU IS
SUBJECT TO THE TERMS AND CONDITIONS SET FORTH IN THIS AGREEMENT. IF THE
SOFTWARE IS INSTALLED ON A COMPUTER OWNED BY A CORPORATION OR OTHER LEGAL
ENTITY, THEN YOU REPRESENT AND WARRANT THAT YOU HAVE THE AUTHORITY TO BIND
SUCH ENTITY TO THE TERMS AND CONDITIONS OF THIS AGREEMENT.

   1.  Special Definitions

      a.  The term "Android" means the open source mobile platform, software
          stack, operating system, middleware, application programming
          interfaces and mobile applications under the trade-name "Android"
          distributed at Android.com.

      b.  The term "Android Applications" means a software application or
          open-source contribution developed by You, designed to operate with
          Android that does not contain or incorporate any of the Software.

      c.  The term "Authorized Android Enabled Device" means only the device
          identified on the site from which You downloaded the Software.
          The term "Software" means the Licensor's proprietary software and
          libraries in object code form, designed for use on the Authorized
          Android Enabled Device.

      d.  The term "Authorized Android Enabled Device Software" means a
          packaged build for Authorized Android Enabled Devices, consisting
          of files suitable for installation on an Authorized Android Enabled
          Device using a mechanism such as fastboot mode or recovery mode.

   2.  License Grant

      a.  Subject to the terms of this Agreement, Licensor hereby grants to
          You, free of charge, a non-exclusive, non-sublicensable,
          non-transferable, limited license, during the term of
          this Agreement, to download, install and use the Software
          internally in machine-readable (i.e., object code) form and the
          Documentation for non-commercial use on an Authorized Android
          Enabled Device and non-commercial redistribution of the Authorized
          Android Enabled Device Software (the "Limited Purpose"). You may
          grant your end users the right to use the Software for
          the Limited Purpose.
          The license to the Software granted to You hereunder is solely for
          the Limited Purpose set forth in this section, and the Software
          shall not be used for any other purpose.

   3.  Restrictions

      a.  Retention of Rights. The entire right, title and interest in the
          Software shall remain with Licensor and, unless specified in
          writing hereunder, no rights are granted to any of the Software.
          Except for the right to use the Software for the Limited Purpose,
          the delivery of the Software to You does not convey to You any
          intellectual property rights in the Software, including, but not
          limited to any rights under any patent, trademark, copyright, or
          trade secret. Neither the delivery of the Software to You nor any
          terms set forth herein shall be construed to grant to You, either
          expressly, by implication or by way of estoppel, any license under
          any patents or other intellectual property rights covering or
          relating to any other product or invention or any combination of
          the Software with any other product. Any rights not expressly
          granted to You herein are reserved by Licensor.

      b.  No Commercialization or Distribution of the Software and
          Documentation. Except as expressly provided in Section 2 of this
          Agreement, You shall have no right to (i) copy, disclose,
          distribute, publically perform, publically display, transfer,
          alter, modify, translate, disassemble, decompile, reverse engineer,
          or adapt the Software and Documentation, or any portion thereof, or
          create any derivative works based thereon; (ii) rent, lease,
          assign, sublicense, resell, disclose or otherwise transfer the
          Software and Documentation in whole or in part to any third party
          (iii) use the Software and Documentation except for the Limited
          Purpose, (iv) remove or alter any of the copyright or proprietary
          notices contained in any of the Software and Documentation. For the
          purposes of clarity, nothing in this Agreement prohibits You from
          making and distributing Android Applications under commercial or
          non-commercial terms, provided that You shall not contain,
          incorporate, and/or compile the Software or any of its derivative
          works, in whole or in part, into Your Android Applications and/or
          any software/devices created by You or by third parties acting on
          Your behalf. You and any such third party shall comply with all of
          the terms and conditions of this Agreement.

      c.  No Reverse Engineering. Except for any portions of the Software
          provided to You in source code format and except for any third
          party code distributed with the Software that is licensed under
          contrary terms, You will not reverse engineer, disassemble,
          decompile, or translate the Software, or otherwise attempt to
          derive the source code version of the Software, except if and to
          the extent expressly permitted under any applicable law.

      d.  Third Party Software. You agree that Android may contain third
          party software. You agree that you may not distribute such third
          party software for any purpose without appropriate licenses from
          the applicable third party or parties.

      e.  No Transfer or Assignment. You shall not assign any of its rights
          or obligations under this Agreement. Any attempted assignment in
          contravention of this Section shall be void.

   4.  Indemnity

      a.  You agree to indemnify and hold harmless Licensor and
          its officers, directors, customers, employees and successors and
          assigns (each an "Indemnified Party") against any and all claims,
          demands, causes of action, losses, liabilities, damages, costs and
          expenses, incurred by the Indemnified Party (including but not
          limited to costs of defense, investigation and reasonable
          attorney's fees) arising out of, resulting from or related to
          (i) any software, products, documentation, content, materials or
          derivative works created or developed by You using the Software
          which causes an infringement of any patent, copyright, trademark,
          trade secret, or other property, publicity or privacy rights of any
          third parties arising in any jurisdiction anywhere in the world,
          (ii) the download, distribution, installation, storage, execution,
          use or transfer of such software, products, documentation, content,
          materials or derivative works by any person or entity, and/or
          (iii) any breach of this Agreement by You. If requested by an
          Indemnified Party, You agree to defend such Indemnified Party in
          connection with any third party claims, demands, or causes of
          action resulting from, arising out of or in connection with any of
          the foregoing.

   5.  Limitation of Liability

      a.  TO THE MAXIMUM EXTENT PERMITTED UNDER APPLICABLE LAWS, UNDER NO
          CIRCUMSTANCES, INCLUDING WITHOUT LIMITATION NEGLIGENCE, SHALL
          LICENSOR, ITS AFFILIATES AND/OR ITS DIRECTORS, OFFICERS,
          EMPLOYEES OR AGENTS BE LIABLE FOR ANY INDIRECT, INCIDENTAL,
          SPECIAL, PUNITIVE OR CONSEQUENTIAL DAMAGES (INCLUDING BUT NOT
          LIMITED TO DAMAGES FOR LOSS OF BUSINESS PROFITS, BUSINESS
          INTERRUPTION, LOSS OF BUSINESS INFORMATION AND THE LIKE) ARISING
          OUT OF OR IN CONNECTION WITH THE SOFTWARE OR ANY DOWNLOAD,
          INSTALLATION OR USE OF, OR INABILITY TO USE, THE SOFTWARE, EVEN IF
          LICENSOR HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH
          DAMAGES. SOME JURISDICTIONS DO NOT ALLOW THE LIMITATION OR
          EXCLUSION OF LIABILITY FOR INCIDENTAL OR CONSEQUENTIAL DAMAGES SO
          THE ABOVE LIMITATION OR EXCLUSION MAY NOT APPLY OR MAY BE LIMITED.
          IN NO EVENT SHALL LICENSOR'S TOTAL AGGREGATE LIABILITY
          TO YOU FOR ANY AND ALL DAMAGES, LOSSES, CLAIMS AND CAUSES OF
          ACTIONS (WHETHER IN CONTRACT, TORT, INCLUDING NEGLIGENCE,
          INDEMNIFICATION OR OTHERWISE) EXCEED ONE HUNDRED U.S. DOLLARS
          (US$100). THE LIMITATIONS SET FORTH IN THIS PARAGRAPH SHALL BE
          DEEMED TO APPLY TO THE MAXIMUM EXTENT PERMITTED BY APPLICABLE LAW.
          THE PARTIES HAVE FULLY CONSIDERED THE FOREGOING ALLOCATION OF RISK
          AND FIND IT REASONABLE, AND THAT THE FOREGOING LIMITATIONS IN THIS
          PARAGRAPH ARE AN ESSENTIAL BASIS OF THE BARGAIN BETWEEN THE
          PARTIES.

   6.  No Warranty

      a.  LICENSOR MAKES NO WARRANTIES, EXPRESS OR IMPLIED, WITH
          RESPECT TO THE SOFTWARE AND DOCUMENTATION PROVIDED UNDER THIS
          AGREEMENT, INCLUDING BUT NOT LIMITED TO ANY WARRANTY OF
          MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE OR AGAINST
          INFRINGEMENT, OR ANY EXPRESS OR IMPLIED WARRANTY ARISING OUT OF
          TRADE USAGE OR OUT OF A COURSE OF DEALING OR COURSE OF PERFORMANCE.
          NOTHING CONTAINED IN THIS AGREEMENT SHALL BE CONSTRUED AS A
          WARRANTY OR REPRESENTATION BY LICENSOR (I) AS TO THE VALIDITY OR
          SCOPE OF ANY PATENT, COPYRIGHT OR OTHER INTELLECTUAL PROPERTY RIGHT
          AND (II) THAT ANY MANUFACTURE OR USE WILL BE FREE FROM INFRINGEMENT
          OF PATENTS, COPYRIGHTS OR OTHER INTELLECTUAL PROPERTY RIGHTS OF
          OTHERS, AND IT SHALL BE THE SOLE RESPONSIBILITY OF YOU TO MAKE SUCH
          DETERMINATION AS IS NECESSARY WITH RESPECT TO THE ACQUISITION OF
          LICENSES UNDER PATENTS AND OTHER INTELLECTUAL PROPERTY OF THIRD
          PARTIES. Licensor SHALL NOT HAVE ANY OBLIGATION TO
          PROVIDE ANY TECHNICAL SUPPORT OF THE SOFTWARE UNDER THIS AGREEMENT.

   7.  Term and Termination

      a.  This Agreement shall be effective on the date You accept this
          Agreement and shall remain in effect until terminated as provided
          herein. You may terminate the Agreement at any time by deleting and
          destroying all copies of the Software and all related information
          in Your possession or control. This Agreement terminates
          immediately and automatically, with or without notice, if You fail
          to comply with any provision hereof. Additionally, Licensor may at
          any time terminate this Agreement, without cause, upon notice to
          You. Upon termination You must delete or destroy all copies of the
          Software in Your possession, and the license granted to You in this
          Agreement shall terminate. Sections 3, 4, 5, 6 and 8 shall survive
          the termination of this Agreement.

   8.  Miscellaneous

      a.  Governing Law. This Agreement is governed and interpreted in
          accordance with the laws of the State of California without giving
          effect to its conflict of laws provisions. The United Nations
          Convention on Contracts for the International Sale of Goods is
          expressly disclaimed and shall not apply. Any claim arising out of
          or related to this Agreement must be brought exclusively in a
          federal or state court located in Santa Clara County, California
          and You consent to the jurisdiction and venue of such courts.

      b.  Waiver and Severability. The failure of either party to require
          performance by the other party of any provision of this Agreement
          shall not affect the full right to require such performance at any
          time thereafter; nor shall the waiver by either party of a breach
          of any provision of this Agreement be taken or held to be a waiver
          of the provision itself. Severability. If any provision of this
          Agreement is unenforceable or invalid under any applicable law or
          is so held by applicable court decision, such unenforceability or
          invalidity shall not render this Agreement unenforceable or invalid
          as a whole, and such provision shall be changed and interpreted so
          as to best accomplish the objectives of such unenforceable or
          invalid provision within the limits of applicable law or
          applicable court decisions.

      c.  Amendment and Modification. This Agreement and any of its terms and
          provisions may only be amended, modified, supplemented or waived in
          a writing signed by both parties hereto.

      d.  Compliance with Laws. You shall comply with all applicable laws,
          rules, and regulations in connection with its activities under this
          Agreement.

      e.  Entire Agreement. This Agreement completely and exclusively states
          the agreement between You and Licensor regarding this subject
          matter.
__EOF__

if test $? != 0
then
  echo ERROR: Couldn\'t display license file 1>&2
  exit 1
fi

echo

echo -n Type \"I ACCEPT\" if you agree to the terms of the license:\ 
read typed

if test "$typed" != I\ ACCEPT
then
  echo
  echo You didn\'t accept the license. Extraction aborted.
  exit 2
fi

echo

tail -n +276 $0 | tar zxv

if test $? != 0
then
  echo
  echo ERROR: Couldn\'t extract files. 1>&2
  exit 3
else
  echo
  echo Files extracted successfully.
fi
exit 0

� ���R �X|SU�?���I���TzS���+��|TM�-m��Ȣ$m�&�&�4���R�]@Q�u��������*���[e�TAA��:+���_��������#��l��{���=��s�������t{���,z,9�z��2�qzFFVFNz���,Ч��3�d_W�����g�^���]r����,��_g���wU_�u��㟑�����(�_ks�l?�*���Ϝ>�S�����w�����������1h��g���c���?';#��'��;����O �nO��Y��L���a'FW����$%���w�{+���^j��'�'�"g��Ug�$��J����#[6ў�d��[�t�H�T=�Q��hWr�l���'��F�r�H}�U8�H���N�v��8]��]�q�\8�
��!U2U<U�.��Pچ��U�_��$�����<��M[�b�T�h�T��zZ�$Y7�Ȓ_X<�p
Z,>s���^WG����;��ky#�yР
[9�Yc[A�^b��ڱ����:}NW�dR��y����Y��:��}ߚ�+���p�l.�l�O,�I�q�e�d�q���\r_��8o����R8���#�%��2KI1�2c��Kq�dbǹ�a�/��t�y�W�I�o�ˀ*�dP��^�rV�_��z[��T��۽.t�x��Zg�f�W�Zj��N��'2�85U�.*�7-�_V8�̺�\�SJm^��V�IE���tU��W�I4��+�v���U-�׈�{ݶJ\ �#v%�}[�����T������)���O���������������#�P���7$�Gs��_�O�U�jjp��^�o��뮬��MqVM�7��M��M!���`���(t#�k�IN���G|�O�J���e`���? ~���P�oL<��$�h����ϼ��?}z����F�������'?�M y�|B�k�庺T�W�56o�8�\�e�����t^I�}�eKJ����{xj\��1+������lXR���Rف<:9H_VfFV�_�������?�=^�3c�Op�����̌���������i����Ǎ���fvvf�ç�����Ǎ(+sh���(�@�>+��q	��?U�l�.�r�X�Ǻ�Xa����>��!�K�Z�ڰ�c��Z�Վ�
k5VV'��%�/q�bP��ǥ�jX�\��(�a4�|郛���H�E^�\q��^��I2s�f�5�$yeYx�ȯ��f�N,�Fb/*6�k �Z��P�8\�Q������\�������
�6"�52��� �8��+b�� j.J�99 �!��)@V��N	�E���EJ�YQ� na%�A�'���l�b+�(Ɩf��~x{?�����G���p���������n��j������jtm�f����l'�^~u���i�'��YQ�D<CqU�����W�,VyՆ1Q<q��L*E�Y��D1��(N���4KW�)��n`[��f� �@�&��;{��-yG��9���&*_�oD��f�g���9�G���Ҩ)�x-DLݧn/d ��Qs'���6s��߰�W�l�_�_�$U/�q���x9���Lz�ZŻZMj"jD؉�Ηj�A+�RQ+�(�@�W@�&
J@�%])��8c��I_[�_'��w��cW��� �� �AMD��߅ޏ�gXn1\�0�.Bw��;{�����+^%DC���͔�P�kSкw<����/v*�ĵ� �W2����s��_	wB,��?��k�.ٓ�O�@����J�Q̻ꈦ������s��s��/r���!����܏�z�����}k�u?�Q�'�.r�&2��#�%�X�q�8t'G����E�Rn%�~@�7�׮�/��V6����`#�؈�a#G�H��c#'��n6r��d)"yd�R�d\����J�"��H�,�PY,C.���H$	��rP����?�o�n@Ew"��.+�Жk��Ri9�4��]�T�F��A*���43@z� ��J�[t��v�CK!���ʟ����O~��j�P�>��!�L�`�Qnӥ�f0)�KI
h^��"~����Z������~xW?�ݩ���nŇ0�Eq3�:���&�V�f��T�.���M�fT�-�i��ES%s%)���&͎DZ��`ej9����/ö�a)��r1�0k��8ࠤ ��ћ��#��Y��Y,'0/�nA@�DjQ,9��ѷ[Fݹ_�``B
��*U,��Y��{�r�o����6h�����T��q�R쐇�9X�o��=���;8����J��i'v��	`��g6�Y�Ӱ�������Y)1�4jy}'��5ԝ�-��l͢����([z+}x�)17�yl��.4+Vdn���L{;ݫEf���Yx�O�>����U+�0��J��#wʠ���U2�q�M��?��d��%q�||uOb�g���#�z�)����59��h�b�Aw>Ew`TS�V�a��Itt��r����!��1k��B��m3VU��^��E&a�M	�����]������:$1�����nd>��[sE�}d�����6�q���D&i��J��_���J!ݔY�#1ڢ�o^Rk��K|3q�ԑ\�����Ç+%f�S���w���'�LJ�[��Յ$fb��t��Q��S�m;��,q
vI��e"�
2$Fwf�Lc���m����Ĥ�"3�?���K��e��LW4w���.^�:�����B~�-�`�@��B�E�2�[���c��Plp�Y�ș`�a��-��׆�֥�u���e�2���a`��[���m2�*ٷǟ�=�֯9���lzJX�	|UXնE8չE8�ْ7�w6�F����vÙU���ZW�\|yt#znxR�����8�����a{�_����t���sl�&��^��w���������p�%���Q�p�����\8y��w_|L`1*�����S]�B�����×�}ڥf!�z�8��q������������+Ƕ9[b�6��� ������~�s�A;޾̘��0��T&��м�J�3�c��l �m��Z+�y��U�c�;�qa�(��E/���9`��]t�2������足 hk���������͌��/#�iқ�u�,Y	c|�����.h�,1�'�̱n!)�-$�L���u�#d�7��&�O�
:Jx�a��.3�G���BS�������RhK�����ɒ���Ъ�^�(F��7i�~�,�Gw�Bd���/4vi`kh��Զ����J�x������3{Y���xR�a��M9���6f�BF��o�S�\���GO�<p�oǴ=#<��N�[�j�����i��9��s|nj_����g�u��� ?�I63���=��Y�	��
�3'n�8�F^��_:?	�-֊�>�3cEw��,	f�5Q���Y+�w�1���	��8g�1��K):\>oaF[F�t\�K��H��*��4�����#���mx��OC�BsqA�q�fL 0[����ضb��]��7�i��?�k>"0���4��\~�K��l2��t��s����Pso(Y��I��y�Q�q&��T|~���Ǚ�3��YvB(�5��|���<J�^M@e���B��-2��CJy �s}CT���9f��I��B<��9t��1z&�/cXYoS�8uI�e��=�4E`J�*P����5$y#T����u�!4jc&-
�'�����(.�}8n�>�t*>mb`^Eiƞ7Н�
k��L�\B�y�����G���#��ݞ�Ȱ�9�{Ir��U�tI��Y��Mc��&�Ċæ�4�uD��")���BD|�ap�����K�>�y��$�IL���"��O��ç/���c�3#���NcF�UC��Wn�����}��Ӏg� qht��Dl]��������Jm�b�%�s	�c!���{���5�9�.�����+|�7��W����d��}�fI���p�ţI��.:4�S)b���8��a[#�#8�:��؃�&�%4�Me�?��2A-�,3h�
aǥ�Z�ѭ	`���Ǻݳ̛�g�@��f6�J���5&z?:������ �p�����Z ձr<���Q�c�6����7�t�:�h0@K�!����r�[W5��;k��.��b͚8Ŀ	'��ϱ-[���DsI/�pV���Z��wg��_.1��6�猆жĒ^�P�'2�v���\wh��ς��6�ž(������tG���i'ǘѩ��r7��f<'�����v�ᫍ32Gi�j>3�����5���;L~���鶤��ؓ�Q�e�(�v��T#|�5��٦GA�g=]y�@Ǫ�����1u��P����"����!x4˴�t�,T���fS���o2�����|���c�0�5�?ݚ�r�)t'���̏��=�/<b��Ā�|$H�7��K9f�5y�
� ���"�Բce�cj��\|��/��-yl��l�U�!L�8ta�׆��>�w��m�cp�S�c�����料Uw$�r"��޷�GQd���yud&	�!��z `�C�ϙ	4�!���(뵃@Dtx:A��c��uݸ����G�D�%k���t.�]Gwu�����$��=�|���{�{�{����Ws�����:uU�5Iz��s�nT!ҤB:���|�p��7�~c�?QǕ����W%�.�+�o]����!���+�ٙ�H��>i�/���y���c_�{&��z�מ}��22�:��j���͈]�O8��� +�'�����c����c?�F�HM?pL灥����,�;�\P?&2�<o^y4�|���a��%)l�sc=��3+�x�f��U�y�/H5��Y����*���}_h�d�ǟ9��T�ˍďm�{��ܙ������w� �M+��?��NG}�Z�Fvr"�1��o�����8�17�P���q?��*�l��د#*؉FP25o
P�-B�d/��K�~ȟ���X3��:�*0���M��"�b,��{4���6m�T����@�4A�)
���C�ّ=�h� :":)��:zMl�����:�Qt�T48��|i����2f�R��0����a]�����::z�M�(�U�򣩤YkF���,�MX=�$`Mُq��6��t�>o9W=�����_�'{��5��='�k|����#�Z�$ ��[��n�g�"$J93���$� :QD��݄\�&'�ҵ=U��F;Vp��믙]����2���D�v���G���ZZ��������T��1X:�~��C��e,-��`X�i���h���Օ���K��է����Rn;���!O��{'0;�i���D]9��3�B^9�#�У��0KPM��],�,i�Dt���%�*��h(
;G%�>d�W�nn�����4�*�W��Z��y���9׿���]��/�=7bڜ�MMs�����+���{x�K_D0�W�z����T��>O��x~��;�m���6}���B�]���l�& Wɐ�f`[B�r��b��T�����#:���x�4D��Ӵ	r�����V4S��9��E�m���$H�tb,��$fi9���ȣ׹�+왝�>�^���8�b�+�rk������ڤ YhJ��A�i.cԜQA���Z�t}s`�(l4�i����Hg��A��_wzu�kgep�����l���<
0xj0 :�=v�-|
���19�Dfb��9��,���0��!dxu���J�#U-���6���
#������O�F˭�Z���4R��N��X�6���ԡB���a�͑�)�����H��zx��e�\Rr��4��9��<V$��zP�43AP�?%����k�2���֫ �iS�H��SGs�u&��0�I��>�B`v=J��bx�p��?y�����6�t����������}-� ���^f��-i�I��L�]Z�O�����h�z�Y��?�]}�hs�������Ꭓk\_#�D��"I���M`]G���є�Ft�!:i�wj��N���<�`ђ��	��Q�ןK���N����n�â�����p���!0{���ڟ���Uכ����(U��v"��g�)Zѥ�"5��t��%SU��3�8g3j�Nѫ#k�HЅ�y����9�N�ig�Mٟ�~��G
㓈6=y���ܗ����8s+�vkV���Q��_����T29�����N�}^>B���?�a���ձoaF�>55��l�ioi�Gk,��'ڜh�=}����
���J��o�q4{-�і�z|H�� �̎6�+h�v�PhC�A �!���8�"n����g�Y�Uff��3R�赶��ȉ�$.r�D���u"����_�K�0�H��unLq��A�cL��}p>�)?]�v8�ߤ���u<d��+I�~|�8�_|�M6�JûyxG~�$��$��~?v����b��d���pb��>�|
�pB��ݦߧ���)I�x�0z_�����q���*C�/�Qū�ff_�#:���nEcZ�C���qB>�).�e���
��rT�՟��[>kKJ����&����65�o�<�U��?9�D��1W8bU�M�"""c��lˤώ0����/�!�sX��w�8M�k�q������D�]M��㐗����/c-�@������:si���zߙc?�Qf��<u�y��<�����pΨĂ������X�!�I��ﺏ^�߆(�H��*�b-gx��7�=u�=��^��(��=�i�=��g��(o�sz�B���Y4�3#��<���U`Oq�j���&�@w��wyO��j�H\���NW�grim�W'H�ԥ��Q_���N�g��^l�j����p�����{�z&ۆ�EO�#�n�'��%�U~8C �`���w��?H\�;��hza��)�����?��^m$�+̉$l�4�T+���K䛧�5��F�4C�H���b�D0��i֕G9&��y��H�B'F
�	�6�1��T$-�8ܔb,��(��4C�f���6q��*D�fX
8��ñ&e/6�U�M�bS�����`=}���ӗQ��tW�Gsa�-{in��I����s�yD���9q��B�"܅��~1P���+�ce�Z���h����'1��d.���3ȉ6�M>r@��8�?6�b�0�y��D�s�����HH�֩��9C�=Z��S1<8�:��Ӝ50K`�6�r\Q�$�+2�UٜN�xfЫnV��D݄�^D�V�yƬ��󳜐׮���o��l���sP��H�u딹��Ӗ��@�97&p��u��	J�-�F�LL��PH�5�����u��JYV�Q���*��C���WHRr�4�t<Nw�I��q�H�bt
Ru�b��D��7��ֹ�*�f!jq<`3�'l�E�܄u6�Dy�[m��,��s ��9��)'*�`���E�c]�KF+~y��y�қ�$.n�gE˹i9�¥������q���a"ԫq�:~Eyq�a��[U(%4W���x.\��!:D'N=�7�ߩ�G��H������0~YS~�����+�z-�q���W�)p�!���J��Z㗥�*�.W��a2�10�Ws��|�e�
�f�Aj�c���x�;TA� ��	C��F�s��b�Ǽ q��ۉ�3U�8��7$��0�����^31�1��I����?�|�Ƭ��O���{��B�����ٖ	�#�s�îJ5-r}v��ǃ�n�l�0��nb�t]G�� �jR���x��s{�ά�XD���?V���K>{���R�t���c~�s-o:�	��mKM�>��ֽʶ\]����� :��)@����x��]I�����.���|����:��8Y�Kz 2P+w���|\�#?@W��(�Al~H(1t	��8��?�:�_Z���Hu{5�"W��NW�G��Yl�kl��	]6jds�,>0�9�����b�kz�Ж����,�N�3r3In�/;��"�#ב�7j&$��1`���m>����Ϲ���l� r؝�5Zn���#呥��]��3)?Z���I'=6[Wz r̮�9b?L�p20;]q �vv:8~i`�E!C�8�0�@�YY
��7�����9F�\��wF1trP�唦#\�QE:�,�t}�D3C4ېu&��9K�.X�4��,N�Q�9?Ly��|ZV�X��Dm��]����l���$�}Lp�'�9۳��@8��vx,N?��Zؖ'6�ɖ�Et�٬�� !I�c�b�1��$��m��߾1�mѪs!񪷱��QL�:��t�1�0�SЉ"�m�C��$l|��/�{��(�1���	[@N`���d�n�PTV�D�ظp�r��=�=]�{l)G�ϷCX/H�F��8��w����;�&�E��ۮ��Wh�ڮ�����z�9d�v�S�W���G���T+��Pᨲ\r[�.�/���lT�sTG^Fl�3/J���i��w��yQ�٬��3��߭L�9��@$Miхa�:O<Q"j������Q��?���(Ͷk4m49�"jf�/7
~&9��"!r�J�怈~i
rF�+���M��P��9��o�Y�cdO0.�$cg�h�;�iFɅ.6܀0���m��.�^ֺ���ܻ|hiyt8�Ld��9�iǩ���s8B�?㯝��d/�`�nW�Rf�8�2	p
�'�֛?@�h�}S�l�p�m�n_��0�.�R�ٵ�:0{o� ��9{������=O��^�zz���2>)���wƠU���a�MJ���*�Ʌ^�g�Y��С��Ԡaqv�>C�m>r�s(�Sd�\�侻%���~_��%�a��ew-X�1{���9�C2��]R6��{Ǆ���Ն��d]����S��N��L}Y��|`	�'H�4d������Β�?�)?=�+?���W��VP�x�}gϰ�D�H��;	��go\���c��:�B��9��K�	&, ���48���k��QcJ={��G���"y�ȓ�ŧϬ��	.�� R��
lZX/튦o��f�v4_��>��/؜fy<Uf�w���C_�^����zr¥R��ơJEQ���z��@8?��qr��;cS`���}`
:��J�����'�i�+HBCu2���&.zdO��КG|p�����۸.�y��Ea�
�G��[��\�XU�U��n����&2fY��XS��e2�"Q{2��Q����NFf�It,_�(��O���'��nK����ޚ`�ie瞓{<+�nSD�Gc�;�Nc0d� �3�J�Ypt�O�2�i9N�H��*�/�������I�b��x \"(�O�R�/�8������s�
v{�Z�!+"&���>���˦0��R��Z��)�B��WKrBN��N� �����t��Q�\e��^k��/�̹���`��\!X������H��g6��V���J�i�����>se�y���lŔ��r��+��~�T~���4	+���|���~!kY7!�Ц��q�R����Lc:�Q��;E�&B��)�A�%a�z Qt����)��U��{j�9�P�2�؄���"B�N1z0�����U��U�ӯ�;Řk�	�Z�%!'I�����NN
�s�������f�U�Ciw@� 4��M2��������:�����lR�V� �t�N�Z����GX�FA�"�J���;��w͎�T��(HO�� u�=���Va^��eX�Wi���L�=r�~�Ck�lq.ֳ�S���U�
z_v���lf6�Ft^R�Τ`��넝!��ͷ��鑐�N'�O~�7�Y�j�*fi$���H���P|�1}_�-M3E��h[R���<k�9α�.�C�&�����^�Ԃ.�௮bf}�f>:uN���FK��t'�?�Dd#A�b�D�W���?ކ)��*�Sc�ϱ/cՌ���4�m�Q�8�g�;�)@������������g
���P�.��8K���/q4W�j��&mW��4�O:�ΝQx�ʩ��q-+���8�,�RR�u�Uk���U���#��D��=<�ڈ�:��V؆�N� �3!��L�����\|�)w�ȡ?�N>�؋1�d��th��LH��o�D�����¬��x:�+�-��^�I5ʳҎKoӧt��_���}{,ؐ���FG�*n����K���t������3�����CӕY�����4�G�	X�"�e_dct_="�.��l��Fث?�>k��;�x�f?�Wāq�g���D۱h���;���aubύR��D���H�-ċ�8����l)}_}��@ׯ-g,ܸ5b�T��T��!e�^k��l��F�[Cu��/XG�H�}hB������\^~�>L@f[#l���Աώ���lLS��~��!��RDv�2�8ͭCۨ-�X9:D�}	x�ۉ���J,y%�{f���������k���~җ(� ݼ?��3�}%�V�����$7���A:E�A�Տ��Czk�}������r`�9�ۥ��|����ϸ~=�*�r�$Y��fŭ[3��w������X�������p\Jmtj�~=T�.`M?E[_
��P���X[���~�����:�rt�Ga�R�X��a�}�G�hF�#�D�ǦBpQ�"��������xs���S=����f�ٍJ ��<9��Y�6�9m�`�\ָW�A�
�U�>.J�q>��j�ğ1'��>��~F�GQ�� YК6�e��a15��wƧ b������b�U4��til}��]v�b:��3�0��+�ό��&ɓ�Z}�1��
�rY�i�!}��o,:���7"sO6�9�g�3,>���X���۰�0� ��GF،n
�S�l])I��}&|�h}���Dnw4�hd�
�;��w��+���m�� ��(��W��_^Ek�A��R�b\�8&eK����:�?XH~t��C�����eB�d`�,b�vM�OG�&m�aK��!)b�!��`�������;,J���R�R�V>M�h����z4i������V7��yqz �$O�ͻ;o��N1�U�Oj���m���3Чy�V�rޢs~�:H��;��M7˫����� ��>�]����3n1�K��b�1�K��k��!��H�3��'t�b���@�C�D���WaC�9��|���k�$˱���v�z�×��F�Z����Ƹ{����m;V��+���%Ѳآc��֒m,��I
+6xl�Q�Ʒir�K��F��D�2 �7�u����s�Ea����H&�-i�%*6���v�#�[׭�Z���g��~��V����aL)h��q�@>£E6�dO�u�qt�� |ql�|31���^�ĵ��=�e:5&f��)#��7�� -�"#��.�C2�ѺC���߼��i�bٚ��lF��l3�\ ��ċ�y�������W9�߰������h*6�m�7�P����S���@��ߤ���0���<��G~�>e���J
\�V���jf!6���7՜�j�l����W�[,���[����}j�|﹍��3<<�hŋ����v}N$�o�����)0��ߊf_|�w������w����=34F��h��<��o��M�8������G�zqhb����t'��#�\��sg����2�
��a=�
����}����g��|�d��>_ Ј�,��3&����8�{��;_":+�v�D :2r�R�Q��+�L|ŅᆂD�j?�ݖ����5S޳�,4��L<�s��`x��p���i�y�
��7����	8ɜ��Vx-�mV��/�`�je=�� ���6����
�_7پ����D޲�܀^��_ٌ�/�,��mп-�ʿ�*�Y�B�R�ik!|��	�p$�ә��`QL�����=�����H��O�����p��\�0o�1��)�nK�-O��ޕ�/ޗ�y%��}΋�/~����������Z��������RT8�{�O-,�t���E��g�LIH=�e�'��d�71佁Ҥ�
;�41Moc�P�����]|�l0޼Ɣ�6���c����ï0܏/�]ջ4�Y�Q��$z��Nk�����}X 咰6æ�^�C�!�%Z��m:ۏ�d�̲>e�t�	��:�)�N�ܓ�ά���9v��g���@�3��1Gӵ29Ѯ���6�}/QS� =M���E��I�[��z�T��C��qS�"�Ww�������B���}u��ܡ�^u�:y�
>���	R	N=�ړ�5�"�G��<�ܒ�՗IӮh]�7������\�z}��e����>�vL�s�\���Of�Ag�f�W#�p-+w�@ߴ�@�I�{��Y�	�S\oal���Y�&��T9��eu ���I����J����W��|�,�����|�!T�	����&n�B�Cؕ�Ya��&�<l�G�MP���i،"��ԉk����s��X0b��_��������U3���O?x����2����'~Լ������L{���u�c�?x��� ��\���N�vI���\����t�����_+�dv���!�w�����nwrrղ;�b���;���͜u#R�]`H�<x�w�XR6�;����op9�|�²Y�п;�⡳�Kn����b�����)�J���e����2�F9��|7�������Y�f��w_�����;�(� �[�練�4o�|�Ƀ��[d�-T�f�V1�Y��S��v���J�7��?�T����8_�~�?��ߥ��JT�U˾s.�?���/�~������ˋ����)E�/�������2��<�x���;����~���7���Щx6�8$���xF���ǌ��P�A�`��
� ��7t.xp:��V�.�����C���ơ��� �� ?�� ,�`�p��Q 7c�`^% V�.��$�G�9�Ѓe���Ư�M��S�����r��Ѱ>#��_��ˣ����},~e�:��+����_L���#�xeT��k�x�2��VF�=ˣ��ˣO����G��=�������ۣ-z^�I�pl�O�MB);Vv�l�1�w��+�]B���C�X��:�x��R����+<��J��츝/a��#��^�t%i3q�i!1U���R�َ��]4��y���[�p�-�Xѥ��̶�z�
���1)d5|�9��:��eߊ��T�쳄��x�d+�p=s4�儓lVLFY`�e�2Xs��͊O$�@Л�ߠ-�D`�U__M̿���8�t�P$�^�־W���t���!�l�l譯�wќ;��Ǿ�9�U?D��y�N��9Ƕ��p�}��b����9jvҏ� �����y��<�4Q�PV�q���것+�]5�2���ǡ_�T�_Ɵ��re�?M���Yx�>�sW�)�z'�]�UBS%��V�k,�z�%��4��#���|Q������ꕒ���ѧk�;ԫx����PVJO��EO���+��5�H�OW�_���}�D8Bg"���q6s�<��ߘ`>��7�ݱ&�Q6��]���n�bU����a�j��ek�]u�SNvas�1⏔C~����Г�eϦ�-��0|�v�&M	�ELS$"��B��XCv�ȅ���&>��<��$ᯙ�}��#�}�3�Z�F�Z�Ա�p&�Ì�vћ��^�)m0�x-�KV�L�H}Db_���J��t^d�iX�,k��X�x�JrP�����24��8�_#g�b�j��B�ɑ����T6yJ��R��PJ<N���]��7�ޥr���A]�KJ���me�F"���<ȿU!�t�_���w�R�$�KRg<�쎵�����H���-��PY�΁`����'v�\�2)'���v6�TZ��k�p$�e�?�K�@h)Q�,a�j2�u�7�I��^�����٦�|�#I�{��g�&���2.���6��Tl�C�{u��'�.ݱ2I�.�q{� E�'���˖�5G�^�N�x�ְ�EW�A�����&-܁-�${7px�Q���m��<X�
z 4R($�VW�{������M�'��V]k�@���#rgvpҐ�DP׎,�֮LrՔ���w�����}zs���KnoX��/.iX�@�\�۰���齫� d�v�����o5A�3Xa.�܏Љ���:��8D��T��Թk�*�q�������G�8�h[�a�$Q�2"d�
tnlmB���Ѫ��N�!X���~2>G��oΩ-�1�^+[]��Kz�;8���0�~�#nS����������=3 {�9��cc�>��Ώ�P�N�#6�f��^��1�4(��FB\��� a�~�r��>h���lxY����Ǭl�%l��n�]��>d���|�
D�:�cR�`�*L?�ұ�1;(��z����N���;z�7K]���8�h5����}%~�n�x%m�4u=?��?��}���lM$�YO��Q)n>H�P���A��7:M?D_�!�l�m���=;g	1B���7�5 boƑ���:|'��C4���%�wBtU��P�A�z}K��c�P�i�K�6��'�=Ds�D�#1��:� ���V���$]?I�����f�?�j���DG�@��^R0��1���S=8P�*V�� ;� :�� ��q�Qԏ�Q=�Ўa���>,k*6/5^E��l�كև賽l��c��#H���(���	�&�.'����UI��p��N���W{
C��"9���'��ޣ?�qK�F-��Yϗ!���y��^�X���_��Ş��B{ޣO�S�fw��X�Ñ����Q�N�<E��d���K�G�*�Ϫ]����|Vx��";����w�
?�'��u����� �F�60���6���8�����Rīir�RS�y5i��O�:Jd��x�=��|�ƻ��x�]��y��d6S�[��|�r0lO��� =�g�Y%�ć��)�[���;��H�E ��gXڈ�B�o���}�L�N�(K��Jf�t�4x�Mb�b�d�`el�K�������4�Ӧ	�)���|Fw�F̨&	�S6x������h�@zD�5F���TC����@�}����E�g���#�a]b��۳�/�m26�:械Ò>��=i��d�����q-u}��Fߌ�"��h��S:�♗�.r����Yc/Rxo�	���~4M�I!$|�+l=Ĉ�$F�6�tB��;�4��X�T�#D�}�`D�K��|FV�X�xSμ�ȳI�Tng��X�ĹGkWJ����;SY���] f�ъ���푊��5�+i��)}�D�-��ü��<Q���b��4��$}�Ǽ
��@b�Å�AH��ND��łH[����<f��4�*��ߐ � "��8����=mZ(�$��\���{:�߄����N�y#��?͊�e�����Sto<_�=0s^��"�F�n��{��i�^�VG�b��5���8m��X�ɻr�lI���ŝ�9u�a�tf��&���~�<Br�{�6II��6�S9�eZ���ӻ��mժ7���Ik�hr3j�S8WپQ�k�	�t��B���X����qҴ�e�[⑩.�s$p�SZ���|<N�_�^���!C��)Z<�w�#G̏�ev�,����L�RK�P��1t����NUR�eL�0G���	��L���g���!�l���f���l�\N�-��r���qחpR��۝s�L-�s�g}V��r�{ͮې�g�9WB��6=;������1AuK����b�d����J����΅��'h/P`1���[����bi6�<6��v�B�����cT�M	N���6��� �Vѐ,nye̬yW+���B�{ɀ[bBH���m%�s�iX��XRM�����&4`�A��9n�[�d#�ף��
�Iغ�~.2���uH;�1�Kˑ�"���Z\���o�j���@ӏ,�V"�V��~��!/���0�N���H=L�r.:�B�
���f�'N8�c��S��L��3�2#Fv&@����^D+��(��0�)������̳�/��(���e�h���߯y�ɗ�rh;W�Q�;_&����z��/����Pغ��i��|�f>��m��a7PO�O���Y���a�-)�uc����a���Y4؄ގv�L\��0��C�4L��［&:�Іdq�Vc�N�O�ErK1�g>�>�zQ$:�R�'�û&�
Wvm��t�n�I�經�����%k�LlZ�`M�ӯ���>��c��8��*MVߧ��l[���(���n^�$D��9�W����V����g�X��<�B�َv�u����_;�T��c,�"��-)t�
Ь\���E�	�U�L0	���k0m���W�����ߌ�4/�TUB�0�G�h�V�<�tԀg���M$�h�1M�҆if1�X;�tPΎp;u�Mb��!�n��-a@��C����m���vڃ"ׄ�I����X�v����Ř��(f�a���a��(X�"�z���숶6���>^����@���h���I��>2�9�ꆝ�u���o���T���kȜ�Sܺ�w��jYPag툱>p4mA�e6��tS�b�F��~΁�#�dKʘGl´YI��?i�y7)M{Ϛ�����O|��ym���.��8�]<7�S��I.��:��GU�m��ؽ����/�Iô6��z�ѐ-d���5΍4A��7��m�B�����2`��L�,,��H����%7,l6;�Gks�z���������vGl���}�
�������V5��W2|hN�yL��y�i��Mb�v5�Ro�i�b6{���Y٤�(�jK�%2J��v�a`v��h
=�-h�8��F-�N��hG�T�ϸg>��8��m,6��}�o��`)�߂>����~�n��;@��ޛ8��J���uz��l����2�ܷ�ߢd7EZF��}؋��75T�@��d����W��� �x��޳�tX�:0fC0�4�To���Um8$P��D����黉ar�v�*��F]�q�A=�̥�j�!�M�
d�I�,��!�b�1��7.]?�ɘ̻��5��f}��%y��|��=�N�]�+oqy���y[+c�pм3K����lL'��q%sQ�h�T�q�>�2��7,*� S�F^�^SJ�d�G��{�0\�DS�!�Y0݄]�W!�Ze��>ӕ�i����[�C�LL�:o�`X�-���ǡHW�jލ�G��w=D��?H��䡭�`�#Dx��~�D�&\�v�qV�A�ҳ[%�W�k�,e�<Y�Ո��(�����U�j��Ƽ\�
�Vnu�%��}D`L�|�Q��,O���řo�,��
��Q�b���5�	�V(�7�st���L5UFi�� ]ד�xf�w$ɰ�ќ��|�&í�#�Qt�*�����y��r�����vz�ό�n����Tn�H�(�u��%�݌��A���C�3�=��BmYR�@��E7;�#G6�`�Y��/�ߢf��H��v��j�����Ƕ��k�n ;풗<,�96��*�9�S6ϵ9�<6��N�n�����;��.�~��s��u�Q�mtVo��ҹ%����3�o�� h�����CU����w���	�*N�C/�wtAO{-� ��*��
�E�U;]�3y��K�̂��g���_-ܦ�K�+4"�ڭ���:����25#H<�N�O���K�>����#U"��y5V=W��o
��zK��-�G�Ѥ���=U��ZO��5S��e"*��|C���9�%�u:���}&��Q���\$����+����h&������c�/�9��hC�E�&+��~�m����Ɏ����p�2<�Ú
y$�*��$J��BKg� -��
.)4L�4a����--Vr���9���TB�qfm~fsW��YKm�mK��ϢW7?k�ͤ���{.�+C������1���LC�E������`)�׮"��{hϣl����!��ȕ3���8ƇJLW��x�C����j	��y���M!�8�ߓ�^Bp��c�e��ґ�\L����X�8&��������E6F�nV7u���L]�_�@�͈�z��3�)�L�t읅 �?�B?'N:�'���mb��-�ql�������*x�[IV0H�T�7��U5N�~��Ё���C�8�${nQ�6������Ȉ�}�h9��F�J��q�q�Rfy��8Q�H��衡E�Y���ڟ&��C`$<*ޘ<`=�&	�����C&Hf���	:���%T;
I3"�*��I�Ce�[���;�m��x��+�N���e_>1p�x��n������g5�(8����rֻ��'�;��[������ӫ������o���{i�]��U-�����?��=������/����ui�ߥ���������Kץ��u�t]�.]��Kץ��u�t]�.]��K�7�Q[Q �  