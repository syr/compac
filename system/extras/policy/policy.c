#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <linux/unistd.h>
#include <linux/linkage.h>
#include <sys/syscall.h>
#include <getopt.h>


char test[4][100];

static struct option long_options[] = 
{
	{"add"  , 0 , 0 , 'a'},
	{"del"  , 0 , 0 , 'd'},              
	{"print" , 0 , 0 , 'p'},
	{"check", 0  , 0 , 'c'},
	{"uid"  , 1 , 0 , 'u'},
	{"name" , 1 , 0 , 'n'},
	{"perm" , 1 , 0 , 'm'}	
};


int main(int argc, char **argv)
{
	int buf;
	int uid = -1;
	int control = -1;
	int option_index = 0;
	char *name = NULL;
	int len = -1;
	int str_len = 0;
	int index = 0;
	int i = 0;
	int j = 0;

	memset(test,0,sizeof(test));
	strcpy(test[0],"1st");
	strcpy(test[1],"hello");
	strcpy(test[2],"world");
	strcpy(test[3],"4th");
	while ((buf = getopt_long (argc, argv, "adpu:n:cm:",long_options,&option_index)) != -1)
    {
		switch (buf)
		{
		case 'a':
			control = 1;
		break;
		case 'd':
			control = 2;
		break;
		case 'p':
			control = 3;
		break;
		case 'c':
			control = 4;
		break;
		case 'u':
			if(control == 3)
			{
				printf("-u or --uid is not supported when you init or print\n");
			}
			else	
			{
				uid = atoi(optarg);
			}
		break;
		case 'n':
			if(control == 3)
			{
				printf("-n or --name is not supported when you init or print\n");
			}
			else
			{
				len = strlen(optarg);
				name = (char *)calloc(len,sizeof(char));
				strncpy(name,optarg,len);
			}
		break;
		case 'm':
			j = atoi(optarg);
			if(j>4) j = 4;
			memset(test,0,sizeof(test));
			for(i=0;i<j;i++)
			{
				printf("plz input permission %d:",i+1);
				scanf("%s",test[i]);
			}
		break;
		default:
			printf("\n-a -d -p -u -n -m\n");
		break;
		}
	}
	printf("uid %d, name %s, len %d, sizeof(test) %d, control %d\n",uid,name,len,sizeof(test),control);

	if(	((control == 1) && (name == NULL || uid == -1)) ||
		((control == 4) && (name == NULL || uid == -1))  )
	{
		printf("need package name or uid\n");
		return -1;
	}
	
	policyM(uid,name,len,(void *)test,sizeof(test),control);

	free(name);
	return 0;
}


