#!/bin/bash
#
# (C) Audience, Inc.
#
# Usage is subject to the enclosed license agreement

echo
echo The license for this software will now be displayed.
echo You must agree to this license before using this software.
echo
echo -n Press Enter to view the license
read dummy
echo

more << __EOF__
THIS DEVELOPER SOFTWARE LICENSE AGREEMENT (THE "AGREEMENT") IS A LEGALLY
BINDING AGREEMENT BETWEEN Audience, Inc. ("LICENSOR") AND
YOU OR THE LEGAL ENTITY YOU REPRESENT ("You" or its possessive, "Your"). BY
TYPING "I ACCEPT" WHERE INDICATED YOU ACKNOWLEDGE THAT YOU HAVE READ THIS
AGREEMENT, UNDERSTAND IT AND AGREE TO BE BOUND BY ITS TERMS AND CONDITIONS.
IF YOU DO NOT AGREE TO THESE TERMS YOU MUST DISCONTINUE THE INSTALLATION
PROCESS AND YOU SHALL NOT USE THE SOFTWARE OR RETAIN ANY COPIES OF THE
SOFTWARE OR DOCUMENTATION. ANY USE OR POSSESSION OF THE SOFTWARE BY YOU IS
SUBJECT TO THE TERMS AND CONDITIONS SET FORTH IN THIS AGREEMENT. IF THE
SOFTWARE IS INSTALLED ON A COMPUTER OWNED BY A CORPORATION OR OTHER LEGAL
ENTITY, THEN YOU REPRESENT AND WARRANT THAT YOU HAVE THE AUTHORITY TO BIND
SUCH ENTITY TO THE TERMS AND CONDITIONS OF THIS AGREEMENT.

   1.  Special Definitions

      a.  The term "Android" means the open source mobile platform, software
          stack, operating system, middleware, application programming
          interfaces and mobile applications under the trade-name "Android"
          distributed at Android.com.

      b.  The term "Android Applications" means a software application or
          open-source contribution developed by You, designed to operate with
          Android that does not contain or incorporate any of the Software.

      c.  The term "Authorized Android Enabled Device" means only the device
          identified on the site from which You downloaded the Software.
          The term "Software" means the Licensor's proprietary software and
          libraries in object code form, designed for use on the Authorized
          Android Enabled Device.

      d.  The term "Authorized Android Enabled Device Software" means a
          packaged build for Authorized Android Enabled Devices, consisting
          of files suitable for installation on an Authorized Android Enabled
          Device using a mechanism such as fastboot mode or recovery mode.

   2.  License Grant

      a.  Subject to the terms of this Agreement, Licensor hereby grants to
          You, free of charge, a non-exclusive, non-sublicensable,
          non-transferable, limited license, during the term of
          this Agreement, to download, install and use the Software
          internally in machine-readable (i.e., object code) form and the
          Documentation for non-commercial use on an Authorized Android
          Enabled Device and non-commercial redistribution of the Authorized
          Android Enabled Device Software (the "Limited Purpose"). You may
          grant your end users the right to use the Software for
          the Limited Purpose.
          The license to the Software granted to You hereunder is solely for
          the Limited Purpose set forth in this section, and the Software
          shall not be used for any other purpose.

   3.  Restrictions

      a.  Retention of Rights. The entire right, title and interest in the
          Software shall remain with Licensor and, unless specified in
          writing hereunder, no rights are granted to any of the Software.
          Except for the right to use the Software for the Limited Purpose,
          the delivery of the Software to You does not convey to You any
          intellectual property rights in the Software, including, but not
          limited to any rights under any patent, trademark, copyright, or
          trade secret. Neither the delivery of the Software to You nor any
          terms set forth herein shall be construed to grant to You, either
          expressly, by implication or by way of estoppel, any license under
          any patents or other intellectual property rights covering or
          relating to any other product or invention or any combination of
          the Software with any other product. Any rights not expressly
          granted to You herein are reserved by Licensor.

      b.  No Commercialization or Distribution of the Software and
          Documentation. Except as expressly provided in Section 2 of this
          Agreement, You shall have no right to (i) copy, disclose,
          distribute, publically perform, publically display, transfer,
          alter, modify, translate, disassemble, decompile, reverse engineer,
          or adapt the Software and Documentation, or any portion thereof, or
          create any derivative works based thereon; (ii) rent, lease,
          assign, sublicense, resell, disclose or otherwise transfer the
          Software and Documentation in whole or in part to any third party
          (iii) use the Software and Documentation except for the Limited
          Purpose, (iv) remove or alter any of the copyright or proprietary
          notices contained in any of the Software and Documentation. For the
          purposes of clarity, nothing in this Agreement prohibits You from
          making and distributing Android Applications under commercial or
          non-commercial terms, provided that You shall not contain,
          incorporate, and/or compile the Software or any of its derivative
          works, in whole or in part, into Your Android Applications and/or
          any software/devices created by You or by third parties acting on
          Your behalf. You and any such third party shall comply with all of
          the terms and conditions of this Agreement.

      c.  No Reverse Engineering. Except for any portions of the Software
          provided to You in source code format and except for any third
          party code distributed with the Software that is licensed under
          contrary terms, You will not reverse engineer, disassemble,
          decompile, or translate the Software, or otherwise attempt to
          derive the source code version of the Software, except if and to
          the extent expressly permitted under any applicable law.

      d.  Third Party Software. You agree that Android may contain third
          party software. You agree that you may not distribute such third
          party software for any purpose without appropriate licenses from
          the applicable third party or parties.

      e.  No Transfer or Assignment. You shall not assign any of its rights
          or obligations under this Agreement. Any attempted assignment in
          contravention of this Section shall be void.

   4.  Indemnity

      a.  You agree to indemnify and hold harmless Licensor and
          its officers, directors, customers, employees and successors and
          assigns (each an "Indemnified Party") against any and all claims,
          demands, causes of action, losses, liabilities, damages, costs and
          expenses, incurred by the Indemnified Party (including but not
          limited to costs of defense, investigation and reasonable
          attorney's fees) arising out of, resulting from or related to
          (i) any software, products, documentation, content, materials or
          derivative works created or developed by You using the Software
          which causes an infringement of any patent, copyright, trademark,
          trade secret, or other property, publicity or privacy rights of any
          third parties arising in any jurisdiction anywhere in the world,
          (ii) the download, distribution, installation, storage, execution,
          use or transfer of such software, products, documentation, content,
          materials or derivative works by any person or entity, and/or
          (iii) any breach of this Agreement by You. If requested by an
          Indemnified Party, You agree to defend such Indemnified Party in
          connection with any third party claims, demands, or causes of
          action resulting from, arising out of or in connection with any of
          the foregoing.

   5.  Limitation of Liability

      a.  TO THE MAXIMUM EXTENT PERMITTED UNDER APPLICABLE LAWS, UNDER NO
          CIRCUMSTANCES, INCLUDING WITHOUT LIMITATION NEGLIGENCE, SHALL
          LICENSOR, ITS AFFILIATES AND/OR ITS DIRECTORS, OFFICERS,
          EMPLOYEES OR AGENTS BE LIABLE FOR ANY INDIRECT, INCIDENTAL,
          SPECIAL, PUNITIVE OR CONSEQUENTIAL DAMAGES (INCLUDING BUT NOT
          LIMITED TO DAMAGES FOR LOSS OF BUSINESS PROFITS, BUSINESS
          INTERRUPTION, LOSS OF BUSINESS INFORMATION AND THE LIKE) ARISING
          OUT OF OR IN CONNECTION WITH THE SOFTWARE OR ANY DOWNLOAD,
          INSTALLATION OR USE OF, OR INABILITY TO USE, THE SOFTWARE, EVEN IF
          LICENSOR HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH
          DAMAGES. SOME JURISDICTIONS DO NOT ALLOW THE LIMITATION OR
          EXCLUSION OF LIABILITY FOR INCIDENTAL OR CONSEQUENTIAL DAMAGES SO
          THE ABOVE LIMITATION OR EXCLUSION MAY NOT APPLY OR MAY BE LIMITED.
          IN NO EVENT SHALL LICENSOR'S TOTAL AGGREGATE LIABILITY
          TO YOU FOR ANY AND ALL DAMAGES, LOSSES, CLAIMS AND CAUSES OF
          ACTIONS (WHETHER IN CONTRACT, TORT, INCLUDING NEGLIGENCE,
          INDEMNIFICATION OR OTHERWISE) EXCEED ONE HUNDRED U.S. DOLLARS
          (US$100). THE LIMITATIONS SET FORTH IN THIS PARAGRAPH SHALL BE
          DEEMED TO APPLY TO THE MAXIMUM EXTENT PERMITTED BY APPLICABLE LAW.
          THE PARTIES HAVE FULLY CONSIDERED THE FOREGOING ALLOCATION OF RISK
          AND FIND IT REASONABLE, AND THAT THE FOREGOING LIMITATIONS IN THIS
          PARAGRAPH ARE AN ESSENTIAL BASIS OF THE BARGAIN BETWEEN THE
          PARTIES.

   6.  No Warranty

      a.  LICENSOR MAKES NO WARRANTIES, EXPRESS OR IMPLIED, WITH
          RESPECT TO THE SOFTWARE AND DOCUMENTATION PROVIDED UNDER THIS
          AGREEMENT, INCLUDING BUT NOT LIMITED TO ANY WARRANTY OF
          MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE OR AGAINST
          INFRINGEMENT, OR ANY EXPRESS OR IMPLIED WARRANTY ARISING OUT OF
          TRADE USAGE OR OUT OF A COURSE OF DEALING OR COURSE OF PERFORMANCE.
          NOTHING CONTAINED IN THIS AGREEMENT SHALL BE CONSTRUED AS A
          WARRANTY OR REPRESENTATION BY LICENSOR (I) AS TO THE VALIDITY OR
          SCOPE OF ANY PATENT, COPYRIGHT OR OTHER INTELLECTUAL PROPERTY RIGHT
          AND (II) THAT ANY MANUFACTURE OR USE WILL BE FREE FROM INFRINGEMENT
          OF PATENTS, COPYRIGHTS OR OTHER INTELLECTUAL PROPERTY RIGHTS OF
          OTHERS, AND IT SHALL BE THE SOLE RESPONSIBILITY OF YOU TO MAKE SUCH
          DETERMINATION AS IS NECESSARY WITH RESPECT TO THE ACQUISITION OF
          LICENSES UNDER PATENTS AND OTHER INTELLECTUAL PROPERTY OF THIRD
          PARTIES. Licensor SHALL NOT HAVE ANY OBLIGATION TO
          PROVIDE ANY TECHNICAL SUPPORT OF THE SOFTWARE UNDER THIS AGREEMENT.

   7.  Term and Termination

      a.  This Agreement shall be effective on the date You accept this
          Agreement and shall remain in effect until terminated as provided
          herein. You may terminate the Agreement at any time by deleting and
          destroying all copies of the Software and all related information
          in Your possession or control. This Agreement terminates
          immediately and automatically, with or without notice, if You fail
          to comply with any provision hereof. Additionally, Licensor may at
          any time terminate this Agreement, without cause, upon notice to
          You. Upon termination You must delete or destroy all copies of the
          Software in Your possession, and the license granted to You in this
          Agreement shall terminate. Sections 3, 4, 5, 6 and 8 shall survive
          the termination of this Agreement.

   8.  Miscellaneous

      a.  Governing Law. This Agreement is governed and interpreted in
          accordance with the laws of the State of California without giving
          effect to its conflict of laws provisions. The United Nations
          Convention on Contracts for the International Sale of Goods is
          expressly disclaimed and shall not apply. Any claim arising out of
          or related to this Agreement must be brought exclusively in a
          federal or state court located in Santa Clara County, California
          and You consent to the jurisdiction and venue of such courts.

      b.  Waiver and Severability. The failure of either party to require
          performance by the other party of any provision of this Agreement
          shall not affect the full right to require such performance at any
          time thereafter; nor shall the waiver by either party of a breach
          of any provision of this Agreement be taken or held to be a waiver
          of the provision itself. Severability. If any provision of this
          Agreement is unenforceable or invalid under any applicable law or
          is so held by applicable court decision, such unenforceability or
          invalidity shall not render this Agreement unenforceable or invalid
          as a whole, and such provision shall be changed and interpreted so
          as to best accomplish the objectives of such unenforceable or
          invalid provision within the limits of applicable law or
          applicable court decisions.

      c.  Amendment and Modification. This Agreement and any of its terms and
          provisions may only be amended, modified, supplemented or waived in
          a writing signed by both parties hereto.

      d.  Compliance with Laws. You shall comply with all applicable laws,
          rules, and regulations in connection with its activities under this
          Agreement.

      e.  Entire Agreement. This Agreement completely and exclusively states
          the agreement between You and Licensor regarding this subject
          matter.
__EOF__

if test $? != 0
then
  echo ERROR: Couldn\'t display license file 1>&2
  exit 1
fi

echo

echo -n Type \"I ACCEPT\" if you agree to the terms of the license:\ 
read typed

if test "$typed" != I\ ACCEPT
then
  echo
  echo You didn\'t accept the license. Extraction aborted.
  exit 2
fi

echo

tail -n +276 $0 | tar zxv

if test $? != 0
then
  echo
  echo ERROR: Couldn\'t extract files. 1>&2
  exit 3
else
  echo
  echo Files extracted successfully.
fi
exit 0

� ���R �Y\�U�?��">@#�"�!"&�0�s�� � �4��ři+s�U����_kTd}Ge�+%�|���J[K4-3c���?���#�@Z����?���ι�s�=������T�U����L"La!"�br�\>H,��CÂA$
� 	�C�b��ب4��x/����M�|����K��<����HDm��!��Z��Aς�Ih[�Nj5��:�A���k
Fp����/�A����h;�?,DD��Fy���<�}I�N?Ѡ)(41n�$�PMZ�A�Q�4�ZK2u%�<5I7�Ʃ�}]��M�Z[�V��Jm F*�W�!a-�d��P��i�8PD�(�7k� Cu%d�r"�ꌤ�X�*4�$_S�&�<��H4Z���/�(�������)	D��
]�Q��J��c)ߞ�(y��Th4ꥃM�0!P�Y�3*�9��$�N��s2õE��bbP?_�1�Xs'��S梙E�	Dg ��ی:j��ƨ���b]�q�ҠF-*M�Ѡ�-16��<�=�K�%ފL���Mb�I���cdRVb��,2R���H�J�I�2H\Zj|RVRZ*��"u4INJ��'j�v�.���h���Q��N�T�����*֫�4��<���DY�&�R�A��!z�a���F��S��"�x�Qi�jZ*��5%-N����5x�F�{J��`�(�pSqu�h�JTj�6"e�J�Ɛ��k�2�[�A�T���m[k���^s�I���_�.�X�>��������a��h�8�m��m�o����������㗧,*Bϡ�c�ޠS��4��2�J�����^A���oP��A��p�8z����w�����=���?���x�k���I�ǿ�f�/�q���8�1�aA!m��%�������ח(�~Br�t�~��V���(�.��]�**�M�i��㲲�qɊ!8<�5�q�1SKD!���nZ�����/�z�F�;��p����$���(���-��\����q��_"
� �y�(H"�H,�HB�������������)�\�o������I��u��9R��*n~VH��P�WGO"�"$r�w�[/����B�\h��d�;3z��u�q�f��H K͠-��}������[�:�c1@�@��ˣ� �?�=��g�i�;��z
	�& e�y��?μ�ѴMo�߉�Wa��v`��	�y�����A��Q>��'�	DC�~�G"���0=f�W+���_N}�!a�{�����%46
�a�v�>a�M��hDͿ�������%C������b�@	?,S�v����q��2�vev�����[��%��h+��\��ٍƘy]�����>�Gi�PΦ�������鹫�����D;��SU�$�3a����t\�[���'�����;�C��GQU��|��h��C�D���\\���|z�'���X��f̙O��z�5��ӎ�W��[]K������`�O[:#�
0E�>������v�/J��c�kž9��x���55�S�ߊ�7�|>���|������B|���5�+��4H��&���x		��O`Ю�z3�Mr��8g7�r�K��o��T��е�͒|_�d��y,5�l����3�_H�W?{�Y�T��l�~���^��1kƕĜ�1hЈ��!a����������u/��4��1YjƞH�w`Fwo�* �R�x��GM�u�1�=�G�!p�_,zs���_,4�u�m,w�y��m�`��	��u�����t��B;?pq��Ǹt��9L/�<g���U)��??Ӎ��|��qw��jz0gǳ�#n5v8�0�/�� ���`:�E��kE��Nn��>ܺ;���~�� ����8�P'�����Ki9��vQ9����dg"�q�¶�,��V��>������l��}c5���`�6�=	�����x����W�5���������\��yM�Ԧ�Sc�oll�Le��ނ>��Z��㵇����|�z<g� �"���$�B���焤��NH�"j'��B��c[җd���GZ�u�B!� �!:�O�s��X��#��p�!<��,D�QF�xF�4��D��K�|�N����q�9!�w��P��;�s3��A��Z�pk�-�����X������N�l��C?ޯ4_؏_'����r:+�x^9�K霣�f;9��N��2�W������k����ݷy̅О[�"_~�M�Ѿ<��iN��M��H�}�(���������V�.z�R�z�#���Q�_FP;8~?��;3����1؝�*��b4��9�QZ��:���m{S�)-Gz~V!Md��g��`�k��<� ��<����<�@^_5���&�r{+֙�1��č�*��8��jl\ts�MZ#���Hk�5�ƈ;2w�������.��v��T�h:ҞN�A������h9�fF���e�uH�����h�2���5,�X�Xg�csbs�R���,$}���.!|�^0N�<u�($���*�[,+"���l!!�1��TO��Nȋt�
���HE�Q��9�G�#�r�gގش���E��)��$���=�#�>q�Jm�ZJ��"B�#�ħ���	y�Ct��"u6�A�a8���I�4"����Se`��J�,�Z}y����6����y\F��Y#�=Ņ��\ȱTbL�y+�a��	�ӑ�Q�B�7�W�}�!O!ҕ���E����݃�>����4ՠ]u�gڴaDs�)X��H_���\�}��@�E���M�:��_g���|�cB�~��-cp����m���:�L���mǨ���7�'r^·�`9/�SA:*��|N,/�b����TЂ.�o.o���*����TЂER�����]?��b�vs$/w�Qg����Qg�l�k/gt!����Z�ӿ��uo��o%�U�ns���?�`�MO�?è�A~7����o��8��q�?��1y۸�aa􄃼-Y��]���"w���b�����u�Ɵ�zF-��������,d�~�v���?��)�v�&
�g�r�p��:{�"#+{X\B���(�~�ܐ�R�+����3�=���9��������}|���+�L�ԸyC
x ��wO��=��]�XΧ+��qf�k��3�_��A,F��	x���ӽU
"���%4ũKx 9Iq����	kǀi��� u�<P�����[�e<x��A���e p�@H�J4�I�����='��aP�U�8:9����'�)�|	*O���)g�Qp @�`��?LP�-h:�;G���P �P>5�G�t�(�
N�
���]��Y ����{��G�*��w��J�'�v-�����/t�P��^��~�@l��D�_nC��~/�y�Ϳ��hM�c6����Ż�Q��D/��zx�� 6@�a�_��ߋg�p@����T9�YS{rF�Ͽ��͏E��u��+1(������켠��x�H��p��A�v�z���=t��X����數��N[���v��{ao��{];�f���#w`�#[;��Z�rW�~�ل0#�#�Q9�Ꮀ�!�#r"�=aُr=B�p��S̡߄�X��aI�4��ߕ�C����lF�$����; ��{2��K����#C7�;�7A�}�I��Y �����Y�G�V��������I���3�#轛~�^�z��x�Ϸo�db��E�_�-�����V=��,ceFȯs.G6�؞����>_{���Of����#g������s���_�Q|��҂�y���W<~Z�S��K�>Y����5ӵs��W^oc�ntlx�aUõ�����������s�74��я7�{���"�����{E���g�]����+6�x�2ըգVG��XX5�]�{C?��lӲ-˶|����w��}�۹tW���O6~zkO¾K�b\>�R{�v�!�#���*>���q��ۿ�>!�z���'��N�:����*���Asq��[g6����^G4���5c�w���'��\�����C��.발������P�U����c�'�@G1k�֭���U�ō�����Gq��\�G�%5Kj�Ԫ>;l9l|�����s����s���}��8-�p+���=!�b(����f�/
���В���/�O�2��Y�J��)���_z5vf�kU�U�ϜS=O�Ɯ�7%K$KKoU�^Q�r�*�ڼ��ޟ����oN�^=k�]G?�;��O[����C+��=���_��j�	�7�3/�n�nՏ/n8����ח\=|���G[N�؁�籨����t���b�K}�\��k����,�Rq|����m��k����sO��v��_z���K����������]����k`�34N6��e�P�o}���o�H.]{�n`�l�I�α���뷕��,��3k�ܲs�(���\������&n}gǐ���^�g���ɖӇ����˞�����k����-9���ok�|u��w7��ܤT�~����^<t���:�j�v���RqeΕ�W�O /�l�����[ݦ/��.���n���^e]���B�ܙ��^�2u��GO�f��ԥ�y}�����zz�[9)�5<K��ۨ!c�ӯ��L�v�Ҕ�����C�M���j��1�/�/�b��;^
�q��M��O�R4Z
S���5O
�)��I�b����I
˧I��,)��'ӛR�h����Rxy��o���RؼS
�����!);.����������}ݔB���u�A����<_�d�,�A�x����(͓�U��}A��d�1W�ޒA�52��$�ɟ� ��Nɠ�2ƹQ�� r�u�{�|�^�1�4�Ų��R���"M�*7{�3��Sv��t�Tp�Ň��v�"�����܉�ױ�	�~�?fl���|x��5��� ����ֽ[{��L�goo�p<�����+�����20�;tk�BQqID@���"QQ�������"��adX�E�a�(�"�0AE���("���(n�w�f	\�|�^��b��sj;U�>�]5]E�s������|z�=������N�6�	�th.�E�k������4���Z[[,�P!�����в2���w��^���#���P��}�x�1yc�\������̷B��R�RSE�;�_�����쟔FTp�"��-K�Qٷ�De�&k��?Uq�(5.qAmQ?+u���4?��>���[�+��u�Y�J?C�������m����y2���臖Z��or���x�N�����UeW�V�o��{Ep��H..(i�����s�gG���ata^�K~���ܟ��;Z�%�9�0?cu���串�:��%W%e�ޟ���Ę����nF~��K��?d����`�u.���{�����%�vj`�Rfm��y��V�u���T��:HE�_ᱬ��[<�k�:�Ξ��,�0,�L��X,��q�=>���!��&p������edr��Ѽ�#?�eN}t������tό~���ܗ.Y�h����s��qq�=k�:N�2i����-Ǌ��,���ƚ���=r�`�?`@e��]��V��6!;礿+�յo�oU*e+��c=?�lE�Þ֡��gR��!NS�����b�kΒYt�7h�E=[U5`5N_�=�����ʼ��'Pv�ON��D�3p��$��g�e)p渕��\���j|u��:�:VSv�)+uS��E��g�c�)&�ܚY�/)�ˁ7���.=�;�;��Joc��&W�4��底�~!R�%eT�Y���e(�����4�:c�j���Z���
���=�<�j{ܯ@�y���FK��N�)qM"����Q�a���yJW[$z9�����L�m���z�)"����΢��+Y���tu�E�,�91b�������t~3�zՑ35a��*�˶pD�'l/X_�-T�8�n�
q��`�·⚷߱C�.�����>ǭP�>�u.+S�|�.�&;J��]��q�[d�4��l?(������ 򏈧Z�
��g)�ɹ�ǹk)|0���k���BM;6���"v4��Ĳ�������Bf��j�c��a?��O�>�j5�;��aM��k�ld|#�r�=됽�8o�9�3#���X��5�Ĝ3ߪ7Ll,	07yg$�����F��C����F���ƍ�=@�]�i�)�����cm�d��*����V�����-�[2[��z0���a���u+ֻ�箯��9l�S��������{���/����� A�[٪7��5���?�3���o��_s���������a�~�~C�W�����?��ȿ_�� ۀ9N�h@�����Tǟ���cv�8��I��`sC%�b@BK�1�`�s� ]���eߨ�b���Xa���CFDQo��r�ձ*�uڒ1�r��	�~W�M�Re�jqa�9���1�o�u|2�����WX�,l�7.!$���V��K��j0qs"�U�1k�M�Z���8���%����q����d�)5?b5 � �$��rs�KU��Q�#�(��T��ʧŏ���J����[3��Σ��ϧ���(�����e֔�9k�x�<����R���G��a�.L�
P<7����q�p0���~M�z*�W�%S/$;�q��A���/f2�f(�T<�d�|_9T�D�R�WMC}�z�:S�U#Ki��<��Rs����Z6�{��k�tBu�uLt�t+u����]�� X�b"��#�	&iB��AdYI�~�K�K��4z���_`�]ux}�󔖔e�Ji�C*i�)�)�ϥ���j�r0�9Q;�7#%�I�Y�N�{2/� �!9R`��+Xupsr��XRfBv�Ք�I���Ȉ7?��UBj{?�K�[ܭ�8��w�j{_��ˣ�E��)�!�!F|�������a2L��M0c"��Jse��P��@rB9�<b
�\�(T�*!e�2�*T�!u��Pip5�`M��Piq��ZH��-�F:\����
u�WO������B��@�H�K�H!YAے���{�9���H��E�{����v���递�5 TQ�W]gh�~w�m�����ZNusZ�t�Ub�\YQ��j~�a�tM[u]�𚡵)5�j:���)X���΄Q:�����}iZ5|��9�<���WS�t}=�<�C�!׆3JVa�6�q�L<k�C}����143,�f0�0u��a�лC���8�4<i�c�������c`�2��rlw��>�4�@5_���;n�������18�*r��<͢k�U�?��Fgȥ/OmM�9�/^��8fiL��>=.�-�M3C�ȼ��܂�c�Gd�&o���03���ϑ������κ{��.���Ba�CO�n�k�n���;ʷ��
d�l۾������]���2�{��6?�9��܇��tSҦg�6�xglP߰�K�����k��!�#^�^~^�,��y����\��3���w<?\���O����zZ�
����=���zy�ב����6op��r���
���KC˥���AH[ӣ��kx\�b�5���Kˊ��.8�-l9R|�.�Mx)+�P^ڏ�جH����e����l@_���#������>�����A�^�:*ӫ���^��~�^i��hy�>߳��tG���B��Ŀ�[9k�%����A+��Yo���D�F�����,�2e�M.���	�7z�֬[�e��ʰWV�X��j�J͕%�s�M[6ͽ����β��T����~�U+VYk�f�	�=F�Wٴ��oԎܨ����u;�� X;L3bL�#^�(�ǐ��}B5w�ٹ0tX؅�-{<"������Q�~p�ۈ�qQS���-1�{�"�b�"U#���܌��L��i��<0/�4u{jlʵ4�,׌�4�����y~�:�%���o���;ѿOYg�1��E���;���;�(��ߓ��ݔH��;�]B��E(@B�8�wX;�����nc\<�z�O �^A��v�z��9���@�Pk�Y��f��2L�O&��F����$�^DbN"��,H�fuLIL�2�r;�i���_4�'q�s׶x�h&���>!p�g� �ة���/X�ċ�p�21o��ԙ��I��<��5$q��M��9{I\t�$�3?;l9� o���@`3�s�<mC:���E�@��# O���AF@�,����F���#p1��H`g��m/!��;���$�哸T��vA�P/!���Q�V$���n4BI\���n�}��[5�<�����D�X�N/��_���� ��Bb��`o-��}%l�ñ F��z�����C��ϸ8�'�q<輗�%�n� ��\.~��Y1Йp�����l@��6�DrV���y�o5��d�C����Ȋi�1p�	��u$��qz;�2B>l��#Ty�A;���Vg�P����t�sx��u�@���V������2pc#�VM uxP��:B��_��@�H�%�
�3S���@GL �t`,``<�`	�X&v�sC��$�:w�`$<,��K#SSgąɫ�e�'G�� k6���W�>�g���3�R�\�ĚfՂ��@�4ȯ���6�c�61S�v0N�0p�B�P�c%��@��5풵5u(l|���1�:e����/�����<G��L�Yp���Ff�P��b(�:P���y0��A�S, H^H$� g&袮������ud�2����v&N[���x1��	�����a솱���g�e�f�/��_�~���o�1��z��Q����s��<P���B ʏ�B=���p_@�l�D�l0BsAߢ%�V�]��k��?m��p|�|8hF
�r���j [��У�|�8A�͠�@=g�؄�!r<C$���س��;����R!_3У���-�G$�	�R�ײַ}K�w&n]@��h&6����N����YL?�3!�p���>ab�&~�������+���X�6�������6w��@1�-��Z�)��c�I�:�St���g]a2�K��ߝE��9����}�6�^�w��}'Ė�}���z+)[g,#p����_��������}V��1�N�WF?4�/:�#F�4jf1ф e�>O;���@r�SDA*�}��d��>Q�ܣ׉�xy}��r�v�{�O�(ݝ��|�.;��������B�k }�.u�O���2�[��N�վtW��T�O�îvx}�'��;^2�t�~l���%__=�u�������i�N�<2���� ��'�������~�ʆ>���A��ӷ�ϱu_|����������1�/��~}�g�><�3x������A}x�g�|�/��̓�������?���:�3�9�V��u�����ޏ�Z_�O����
,�����/�9��HuB�z�D��#�����Lo_ꆠI7ܓ�K ����	̃� P����Ð�I�l!��ظ����H�_C��$v:�CVN%�# /�����t��X�L`���8� ���Iv�:� 
�/ ?���L < �s�3pkI约9���)$�N%q-�N��o�,�?Q��GD�+�M��~�9��ĕ��$̟	I0����֢���SqB«A\�Bw�s�]�k������?�a�Y��5�t��f��y]S��i�#�ŒZ_K�e7��m^_�H}mt��{g&y�n��9}}���7�����w�{g�|&c���t��V}����=%��IdBߛ�o���,97������?+�gs5�fJ�~���8��{a��ڏd�=��g��� [���b��A�bC$��M\�����ppz��t&'��LO��g��f<���}܀�nV>�bך�����7��{��@q�y|��[������|��7
~>k9�M�#Y��|��c��꡿Ѯ��ߖ])u����<�����8=�:��¯vԔ\���t3����M���)�_��?Ү�n0�c��Fv�.�%�|�_@�g�r�:.��E��_ʿ����d���\&P���!���g�H�{���*���[(}�r#��Z[��|��`/7������<i\l_W?1^ă:nnY�߹ �6�ٸ�����Vϖ)F�`[%@}��N0��/���%w��@�3�l1ś%(���C�;泘�����?ԛ�Pl�MW,��L�^��
��Iǆ���}����д⳧���2x���:`ܷ���\}Y����I� hhhi���i��	��i�$�ˡ�&�GG'�r�W@���l����&х|�4����\ȇ`�ɼ���5�����2�s�����C>��o*�o�9�4��L�-�'`�\�	�S��%�!'y9��7�|6�+a9%���+��k�7��<�����w/���<^�x]oO���=q����z#x|ҿ��o	�#>7��J\w}���gO��Q{@lD����W��>�(��">�&�t��(����-�����?�v�G���A�|��g	��d>|;�%;�������խ��￿�~3l������+J���n�[��U�q����;E�d\����Y���!X�=&�Wqq���bwq���iʟpq�������4�rɽ5_K�[�ŋ�n�+�������I�ǿgPvtx9�;�T.�	�$���?~�ȃH^��_�7��9rd5�w���\�B�^/"cE���u[Edw��t=�]��Y��GD�=�c�[��7c�c "�&�òBXn�H
`®�C�Q���9���lM�]4z/=^f��&��+�g|��D��q�~��{^��|���<E�����:���W�X,"{!�*D��%c��E�|Lv-�' ŗriq	�/)G�Qr���dJ�W�zSxR&�Q���Sr����'�"�"�Wx��.�kP��!'yR�o0=�U����G!���<Ex����xB�wA������Җqt��B~3B�g ���(�y�&�Ť| �x$Q"��Q�m���>K?:�Ix�9�`�`Mʿ�K֤<z(�բ]J�#��IO�|��	��⁉�)Ns/�)frW��"b@H�h�@V���}/]��|��K�*Z�o�$��8%
��	�a����H{~�4 ��A�\ʠ�~L�1�Iu
`sY�9zl^>!'DRB�����jV0N�?�vpŒW��������M4跳�'p���1u��D������f����T<h{��q�.��l�D</��}G���x\��qg85����نL�%l7���l�D�O	�qa��1��c݇Iœc'	U5�+"�`�4wwG��mX�����%C<nO���ǹaX%t؍ S��t�W�PilU#��������7b��J�k߉������U�tOnK���!�=y��)C=ë��U4]	� /�Pu���4�����8��6@���퓠:}kv�������K�}���mh,H��-?~G�$9�{�=⒚-�	P9У�/[����D|���5�� �݁��W�6����)����lA�m5�gx��qoh�?4��g^�l���+�i�̎���=}�z�>���ы����K7�P�n��	�l@��6�5�@���}������1��^��g�׃��!=����z���:wF4�mod���O�56�4.���t��)�¨%�͎�{�ĺ������-�N��BϾB��w)T���y+����=)�wM��E����\�Z	q�A�T���?"t�0��F0�~񤗚�b&;(2 �q/O��"��7$�;c�C��V�vRqX�D�I�c| �<�+�}���c�7h�lS�������;�N����Q76��M,Ӥ������gT9b�/I{!X���\�t�*��t�
�\�1Pxl?]$Ƃkc8Yw)�1Ib*�4w�_[�,3Pػ+
���g`�]�twnI�=(�(d��ql!�D(�j*�B�1[\{P>c�<�Y����iT���b��
���gԀ"�y����*dPݕ��*�Ԧ����bDv�,B�ʺ��(��,�
f�:"��@������x⻚j�_^b�WN�
��w:�����p���%� 16�>�3`��g�l�1[h�KLX*70��u�c�к�I��,��P�	�1u���׿(��B��A�R�` �
��NM�������,�\��QN��Φ3X��m{尞�;�⬉�rj�9a�HL���#
�����m��=1�ǫw�6��:�}5nhI������H@Kl�������5Ĺ��>�"�Ac�U	�R�Nˆ/�Yl�'/��Ab�"����8R���m��2ڎNh�2�"ݰ���	�����^[z�+��l��f�����#��~o%{�T�y���R�T���=m�|M_� }D����v�$[��Nqgф<a��kf\��O�p���B�F�װV��e�P~�]��pm�s����%�;5��~���<��#���ϡ��82gqcZi�V�j���[G�k/i�>Qm��ޭ�:�ED6����Hݴ����E�߬��#9��W�`��@M��m��ĥ�~��gW2���l�Ȇ��z�|���ȕ`��ZU��+��w�����[���\rݥ�F�﩮upȵ�^�p6���v�ç!�`V�U�^e�l���lx�E�YI���gq�wOtj��L׹m���!1�J���^�o�C��\��F;Vl�|j��\�е���G����x$��]�L�:�N���~��$�(�K@tY��d�]�h�G9��3���]�yFF���r��>�	2�^�S�ˌ�tt^���̈��`�nzT���wjzu����.6��q_�e�h�*��k��h��H̹��3[MST�_m�\�*��̙��)��!b�/r?c�a��v�p���;)2b:���9!+�޼��?{���g���:w�H:w���i��K˓*�?��o{��S�\7������+��i�ַ�[]����YT��Pii8(1	L�* Ġ蘰S�<q
g��g���?p�}��DB7�!>�>�[E�j����ԹY4����/ʇ>u^�P��ݸ�����Eδ��˾xa2bi�^�rM+֯��ά.Xf�l������f�;��NG�����⨌@�-_'��#~~�
�r&+k%�|.Lvy媹e��a�c;��{P�U�¡�9������t�����c��9Je?��-��0�����1lԹ:x*7R�%ّsN��b�%n�*����,�0��
L�w���_8�Rw�1)��u��nS
 &��29�#���U�y��.�j� �� �Vٿ������R� `�.vIV�A�c�s"	v#��d퀷��5qg��'��	qS�u�#j�Hߓ�Y.Զ���~^�}������Zq_����6�?��*0Q��ZUϋg�WiD�{�b۫�נ&��U`��8o�+�����U�p��aT��/�z�"U�Γ|"I�@5��rݨ�O�1��*&�C-_���ho=��!���B�yew�z{i9t��Ӫt�	���L)^��,Cr��;��d�S�D���XI�l١�:1�Ó�v��
)k�<��%&i)�$py��n@܁�OM��v!2%p���p�.�>�(Vf0�ßs��Ȣ�CZ��K1�U)�ƜP>'6�%^5t3
�I)��P��b!��p���xK�v�iհc.�)-W,/B��b�F��~��������
b�PDo�fm%q��]����Vq�Nf@ل��eUE����?��h�=3�nɱ����䠼0l�h{�8�7��ؔqa�����5Ҏ?���[g�0�Ӎ���u��}d�-\��#BK���$$}ūu�Ѵ��T�m]��>_�Ѷ�֑��E�֢"�C��4��q',s��|*S[���̊��q���W�T��h�)�#�䇣�,�/�!�r������;. ��?�(�~�Ȉ5)�h=`�v�����N�`W��#T��w�%̚�ߞ;?�����5��'�_>�Z���R�$�]�\R���Xv�oU�1_vT`�eS��J�yX�;�fM�,�u���0Y�\�&�埽���F1f��1���T
&H�N��O�8J����̝b����ҧ�#�9D�;����Ӱ�C���������a������|Lj��V�R?��lQeP��B�	��1����6��6�J�[��,?U�2�o���ͼr	�LU��oW�6������!#|�Ũ9W�o����sU�O���Uf��q8�r�p���#Ah��pI�E�c�N��t+�������x�ݫw��x��F3\��S9a��A\<��,�LjU�@V�x�|�?�@a�N+��榰�).��mb���ì�{����q��&�<.��@��A�O�����Њ`mW�I�ڪ�R���ص�.D[�6�(-���m�B��nܺ�ic�m+�V�*��܀��~��ߏ2�3gΜ9ss�Ε�~oJ|J�c[(���[j��5g��.f�G�5lrޮ6�r��
�4�MD���f6y.��m��S�$җ�}N�W�������/��#l8��2�X����q��<Ჺp�ϳFދt�o���I|ъY/k�����/e�Q[>�1�H�I^���b5�!$���o��mtss�/=m��3]d�����t�����m��Y���(�����n�����-�r��C5��-z?�-Uo?��0���~�=�_��~K��v/�[��NĊU1�_l��U�6��H��7E }����s^�Ԝ/c���v�!���S� &�;[?��-Iٞ	���
�l�|Ϫ����n*���9�j�+|sKlh��'p�])�r̠�\DWENe/���m������U����:�C�s�2�D��AS��fm���9u�m-e�4��*�_=���7Y�]�_�T�`��Lk��Ù)^Z��j�_��@�OS�������c��H���=���*���=-1f]qX�׉�h9-6��P[ם?Mk�\���K�t��O���晚�������5T�Ck�����W�l�����4��W嚨����5k�Q�k�_m]��G�eOֲ�SMT7��-���yr��m(�Oe{�iE�`��t]��m|��NT��:�O��s����E���s��h����?�*}���f�_��KK\�����'����ݗ�����������b�F.���nR��1�\�q$��b{�ln\�N��wѱp)�m,��� _ �r�sܣ,U��S�٠܂Jز�G�=����R��P���NWO¶�n���T�������\2�ljm廛��C���(՗������R��H�p]�]+��O�_�q�s,���;$CZʖi'��P�:�D{@�nC-t�����Ѝ��ƳI�9�(�j\��-�,��^ޕ�v&����`�m�vi��۔]C��7���o������>_be��5�`�C�u-��Ms)�Vw�#�����Fj�؆*��M⨢��.��okON�j��۔�s[Ieז�Ǻj2��NoKP�?`{�&bĕP}�o(�|u��D&TC�=�P������ �� �;0"�(���q��z�O�Qǰ�i��-᪥ezd`�܀���t��^lQ�*Gq�F6]6M!�D�ղ��u
x\`��Q�_�s�C�	�?s�q�Pꮁ�"�?Xv��!��L�ȇ�3���3[�p�����e���%{L����K�X�`��ҫ����2��9��\�>./��`��+���
�����(p㿽�:��2�Q�y}�r�L��\ �Oj�p��-aāڸ�������c���-,Ǎg|�89��?8��Iy46�e�H���)k	.�4&�����%�l���/��)��
���K�8��̪.�V~r��A��D���6f�z�,<����Q~�i	��n���9u���u�tR7(��Aj�<���y�]ti�EN�w/��
ϸ��iI�XC�`��3.0��gc]g#�x0d��YdY�>��]^�&����ӜM��{�L�1YgY˒����;4�l-2�16��|r��+�\�RV[�>���.����T	~����S&M݁���P%�v�MZ��/��B��x�k"��l]t>U����W��5��G�E7vzM���o]z�l&(���p����Xiq�9��3�����p��B�9!D�1_�G����.s�V�f�����p��0�K���ݽ���-;j�H?�lx��A�W)���ȑ^��_4@}�S�e��^W���ÿO�ut���f����o ��v��gT$�N��J^XE�OU^�H�(��H=k��h�W!�=��)�"��{����� �����F��y���|�;���<���\��:���R�2n���t��������T�}�����8�5���y�"l��x4wL�|��|��ﭴl�Ԕ��h�d�ه���gI��S���E�tBZ+oA'�U�� ��ۂ����&���`�6��]41��T�ȱ�\V,=� ʶ���t�9eQ�D����SQ�
�����-�6�}�E�d�����9���O��w��!�+ ��T�H���",pL{]C�:�>��T�5��{��j9�ًf.��k���ڇ:B"õ���a�c�.�z�3x0�ݛo��i԰Hj�i'܏��}S�� t��S�i>�i]K�,$���S�c���տtxu��B|8��>ܞ�m�i�0{=�j�4�Z�NR��QC���P~fJn~*��'��'~�&��RyO'���~���,0�YnEԬ*���g J?�`����?R��M�^�#)5����&B=<�<$��A4#�	޾�)��Kn#{8��G�jC�1<7;��fA��)%LEZ�ŘRR��Zs-�Q�����L���y}b�qTY�4�/��b�g� -��h�0~�Ugg�����Л@OVq��܆�\j�P=^�>1�KexYz�|�C������k�Z��@����mtp�S(��������x���l34?L� 0vcW4�	�N������Z�]�q<׎�������t�6����x�K�<�e|�UR��6"2�Z��p(oQ赵�m�q�P��l {�M5y���-��*�A�ATV	�J��(V��mx#W�jS)�T��P�
��D����1�J���C%�Y��CtѠS�kʵ�$���;�#��y'xFq��o�G r�c\T>V�e��{I:�&6/ͭ2�[�Ow�p��kr{�u
�W�Q��m(��XEM/�����S����`�I��*A!s����J�7U�����[j|��ԅ�����y�ݹ���:"�BlVD��H�b�F1mB�A�����΅�Z�^;� �t��BM��O��'{�o!L����	�����jP�Q���\��r ���[t��3�~�M�n�X�X�S�!{����pH�|$�@�u�q<��]���H[�4st�&ZRKK��)w�{������(s�W��k��1G'j�� h<�/]��T)1G��JxZE�4�����f�b�1�9+������1 ��9E�MB*�f���%�XM� ��?ȹq���*�����&���Oܢ���={}����,]��?���y�����Hg�ЋP���XP1���X	�F���?����B��P)�'�/ ?D��Ǽ:�&:�/������ɾ��l�O<.�����B
����(U&s��ѕv?XU�U?V<��B��aS���ۮ;���{�y����N�!�1��.E�K1եu)�.E� �U�,1]/ՠ�(�Π�5���v��u�(ӣ�ťv��P��H7��Z9��("&d����"^�����(�h0�'�*-{�t�˿B������k���Q�ˑ
ʮ*%l��7L�Mܫ����2d� q/�O9ݗ_����pyLy�m ��dG:�FP 0/� R�C�|dG��>/Az��i�An��Vb)�FUo��u1_�8iJ�w�b�n(�d?��*'���-��)��8�!P�q�$���	�,]:�fG�����?IH5�sņ���m+������W�{s�|�ό+~�.��"�
&׬�G����2t�(�@�	��9WNy�CXVZ�B�@S<�U(�䰽z���t#��"s�N�܅�����{�:�EW9;��^Ӝ�����W�ʋQ"��Xd�oM]�CQ+ק����-����]~���.?Onf���0���b �!}�W؃,t[�&��~��{(+5���MlKi�b>,6>�6�bҶKtƽ$���7ZI�F�d_��?��#��|b�Cjl�X�@�i��e�
��0j��ԉ>G�-���g?��A�9��N~/p��N���s��:�a�����\�1���Q� k��iIw�:�b����&�2�`zm�Y��-� �76����z�L!��|�x�.�Z ?Tɯ%�?{���7_��4�t�\W<�_uʻ���W/�G�-�Z������x���Z���.2�n���P%����k,AY��j����w9��G�_��sV���@I����By���������AZ/_�L���`:i�|c�W��
�_w)�2�x�=��b������>�뺛��y���Km�'�.E�CgSě	�"Q��z34��['��Vy�I�U$i�E�O�Hʛb?��D�v����������G'����ms���O����J��>�����ƾ�Ka�'[���ᠼC+�-�A�Yo�u5�%oqP}�1Z��7z��r�
�r6��[�z��a���My�_&1���N*Ni��[l>���������5�r��^Y:p<�~U�v9p&׹�{K�>���R�i.U�����/OW���	��4��쑳�%�ܽƹp��U���W?'oV���W,�!��z9t	f+,A�)�ȍ}졏���`ˊ��7�Ɂ���UdGZJ���z+�Vo%G��j�V�B�TǓ��'��+䗖��Oʯ>%�U,oXf�̣xڥX�Q�v(V�%��ԬXh;����CiSH�g��^�9(H��TG4�"��;/"�sjñ>i7{��X�r��������!���(RV��r��t�X|Y��f�l��6��/O�bdtx�g�[r6O<I�<�%���r����$�9�*����LTl4(qw��ً͠���9�v�)�RQL6ʤ��P=n�9�T,�'���0$��[ ��u*��O�(lxH��bşH|y�G9��N��ܩ%�d�B�wi�Ck= 
�k;(�
G��6E���ݲ��!7�����[���l�k�jS���v��!]s�t���xr7��F\�-�(n�e���D���S7i�qo([�|���5h �W��pʡ����Y�bEQ�byn���V����G��chΥ��Z�}z�BPc;nZ~��BX'�s�ʣ]�R�\���j��"ء������7�x6�X,����?aV�Z|l�	٘A1+t:���EX��Y��3�5�.�l��q'x�]�P̑���%�d>u��P�0����K�PH��lt} �y�8�n� ̆�;p��L^,�<թ�П8����X5�����?�6Ef������f8�w����s�ZFU���]�t�mUm�z�����tE���Fߨ���ý�c�����o0���Ec;�Ʃn����`�2�*jHsP�5 U&�絶�q
��"Gt�C���\�3�m�(��>п�5@������@6V���ju�Rc{�zO)�:(.V,�� �����֖ӝe\����{�EO���V���Egʲ:A)��s����,�,K���!ς�u7��|���gV���8F���.�}�2��~O@C�֭���R�v�X�p�n;�~�S94^�s�^g�owѝ�s.~kp���C�͸������o�e
*�z��R	�_�{'�jRέĜ�������zRWL�j�����&u�=��W���WYT0�
��$����	�~�xP�X|�2�G�<��Z��J�����p���2E�Kl�&�=�}m�U�V��i��Ȳ��c���N0�2\o������ц����d/�|�}	͹�L�����6��.�ԇ��@g\Y7iʨ0�X�~̛uo`/�Hu�6t{�������2t7T��Df�­d��b�2N-g�^#�k:�vnK�t8`�R�����5�-d�m  �Ŭ<5��`t��K��@��P��s�?���
�R�
�U��O�+)����[��|���|�<��lL����$/��uIt&Q*�I�oѓ��>����Y����s�S T#���Pw�? �Xz�t��ֲ�� �O�16Z˨�I��gU�9���c>�Q��N���Ҩ>��ޚ�]�nv�s���\�p����}���6��#+�v�:����gcF�C��D�~H J�d#H0<�t��Q����`iP�y2��/G>Yh��C���A+��}��O��ṗQ� �ic�73z��<>�K�OX0�/���{
θ?e���#�O�w�b���a�%˞l�'�=�-	���g���f�ycбЩǹ�+P�J|�M
޻O�"�ϼ���'�{,,V�Zu�5I���k��?��1�g���؋��y�D���V�h������s}9.һ�#�'������?eڬ���J�U�T9�m�t��	�4��3:�4ݿ���c�u�P�l��N��ƥ�=��9�D�%�f��`�m���O�w�VedcU7o����Hd������3�f�(���p��(�53Zo���z�=�w��0�M��:���Ol����Ĭ:*�o8����>6x�BRy Qcم�oM�7�h)J��emVd�>:ч�\���d#l��7�?�����R���C�>�!��C���J���=@#��)Z��*S�Y�ڣ����5HB�:Ʃjp��GsA���gPhZo>��Qz��7�gql���t�|6["�9����)H�`>�j��<��P���2����xd�a�v�#�ۋ���������!Ԕ�8_o
w�r�q�YY�j�}<5��;:T}�	[��|�ǵ�w6�0�f�֟�?����|���������|��(���I�?�C$���2�4�O��ܼ�;n�aP5�)c ���oJي�����#�|`�U���P�(ĩ�h��uT3���I�֧X��i��"F�/l���s��'&\L�����Q��hb�S&Nhd� �KA}�v���m(|�*h��P��l, (A��� ��'�����|]�D6ˏF�i���Xʥn��g`��w�I�g�������30O	~��
r�q��0�����Խ�cQ�������} c.g�x��YF;B�(x��_�`�Љ�?�"��$���W�7�%�1W�\|�4�����.���'V���!B#щ�5����6�w�������b��e2�O��l�~��ؒשh����Z��s46E���{Q
SO ����~�AϮah����C�rY���D�-� �@*�r���d��	�k��[���[�2�. җ`e�K'��0�Osƙҁ��e7��n��uO`���uc�#4�2e �@{Bx1b>�ؓ��C�`��Nę�ңNe.4�dڨ��?;����ݙ�Kµ�ĺ6��T+�$��3_�B����{�s�+y�jR,p���M�7���,���z�q��!X�`1j*��,0�	��Fqq�>o�O�D1ff����&ZiJ�G�J]��=����������ʏ.T-����ړ/�+�>��/(6�l8��b"ǂVb���LQ`�O�F6
[S>ڰЅ��2m/��;Y'�W0Y�[��<��m��:�x�D!Ӑ�����R��%j>�4�l}_��%+�hY�mZp�؂�G�"����xoz�����k��N</_�'�x>Z�
)��&0�R����ny vy �OK�|v�����8� >�v���N�:9Q�c��2�W���nF�����&4�.Zǯ콿w�&�z�K�F�"��rF��v�L����#�·g������k/��gǌ��y�su����"�k�i�	w_
]��`��wѱ [[`��FU9�q�9V��ɪ?ٗJ�����:����Z�]t[~�O:4N� ����me�w1�;bD��Жq��ƗS����,�e��µ%�(�_R	;rw�F��Bg�}�%OV&��ki���۸LC��g S����l�3㑍V��Z�~]�b<�4l*�륕�l��c�p��I<��{�H>ݹp�F��bSl�<���D�B>�3E?���@W�z�9J��%�K;�٢���F��{q4��M	|}���7�����ػ��XOT������T���iZ<�5u�^��{�C����'����epN��4�b٫�N\8#=�䅥�󔧙6\l����l�����N��D	��8%56\b\���67)��^x �=�/< q!�%tч�M�܁O�>8My�������Z�������^J3~� rO��M�@a��!Z;ϴ�E��h�#�P}H-S��`�I�a��F �F#�Q�v��-�KA���m�ʣ��YNS(DT���"B0�ٶ�f�9lK]�`W�tGy��<�\.ҖK`TS��!�9 f�A���,� �����~f>�7>;�i$�=��1��0 ��)C�Z��&�%��4RN�O���#Ez�\�F���Q����bؔ��i?3��Ʃ�Ѣ1��%�	��Z��pJ��\/0����~ܴV���J�W�`Ce
bQ�:�A���,'Xv����s����O�VRàB�WVK3�����;�l�U5������.�K
��]|�3 �����3��@�i-�i�$�= $=���=hSֲZ.Mv�lOT���.:�o�Z�NV=k_c4����A�t��P
/ۢ�RP��80|�F�`:� R�I�z$�1�t7(����q�j$�Z�Q��8ԘFk%`Y�5�<�*����� ������:��b��Ls-����7K��f� ���>0�)M���&	魔�
J��$��|+tH�E�q��
+�η���J��K2ե_a�
	V(.���t�RV�T$L�_�iT-L� �g�RQX�R��%�9Tfɵ�tF�2d��Z�q�8�^�$T��j�����
�,�4����������Y��.:P��q�"���x�i�Yw5�/������(������e�Y����L�y�4�1��%k�b��M�E�3Ǟ���(}�E"��uKT�[�����-�<S1Й�_f9��?u}~����0b��h��6`�T,�l �v1�,�/�^τ��*�el��4����rfCS&�k,�N�Qɚ���
�������t��H�Y-4��\��&����g8ȏf:��Q�3�e
8=�gY�!N���3	 $	�#�|�[ְ�0A��sn<�i�~y��BR���b	����b3��Gް^��K5���Ju�<H�,Zऀ���� ��OY�{	��3������e��l�7�ݜ�y��}j-�������b?TV^��G<fFr�6�OFw��d|�&.��T����j0qZ������#����q3��c�阅���e�y��z�D�D#3N��9��ʴQzˏ���'(�KFf$�y�x�q �Ef��y�st�Ieyո��3�M< �s!��]�.O1Ok0xM.��a���u���fu��v��g��1|o���H ����r������ "¹p%���,�2#�q!K� 	=HB�db�22>����Z1ړ�S�y� ��R��qc�o�qɑ53ⶖ�����-N�L7�����@;A�[�4��b�r}�0׎�&��k�*��Bf&>ݓOz9(!�Sݯ�-� j�7����d�R 4�f�.��1ρ\�L��hFf��y�����"��=�7\��&�پ��}�i5d��R��i���7����u�~,y�i�#���8�Ҍ�^�#��2k����=��19�|���@�{�T`�3������U��@־�����!��k﷖,��<��m38�θ��5QO��O<<ٳ��x���p^��ӧ�d
}�W&{����!zR����
�?�����m��<�M�?����%���f$�=��=Oz�A����b�
/���sl���`�`�W���r�h���k�礷g�9^"�4�?6`?�7�x�S����m� �Y��$fO�ʽ��Nm���1K�Ic���;��ZD�  a�9J��9�k��h�p%���#��ww4p,t�d"�n�1���CG?q�a����j������?O�Ͻ�����0<ͪ7�؃���q��6���XdDl;a X@华���9nZ�L����&�g'�,~X�;�5�n��Ue�L�\��6�B�#Q��B � |8ٔ5���j~)��¿\�Ҕoѻs	��*M1��0�˚���80��n�������#���>JO�B�R1BDղ�~��>+;m*�����I���q���1���dy'�D��om'�%,��)���W��^[t'�M1�4L�38�4�[0�ߎ/M����qS��8GR�qt�yi<��z�j<��3>�2�5�h��Zb1c� 9?���S�1���_��X�0��~s�t(�
�	զpi�_����ҵsMYT���s����c�j��yzn���܎���>��?��Mnc�ݼ����.�Wl[7��C����\K�i��k�Ű����4Ll
�0��]k�nW�
e�"l�x}2�������3ͫ��+���jmM%�c]tӬ�@�}�N�z]�s`�a��d��a I�S���|�����e�]�����7�k���;enL�Tc��@��pG �$�k�c��A����眡a�u�����<�:��KG�Y����Ձ�]P���KT=�]��A���Պ4#cV�fN�xn�,� �q�^��OG�nL�\�����1Tߍ�aXS,�P�߾�����GX�M�~x�����D#�ic42��[�=(��7�IC��I��7�Wicm1�\��57��D�;t���3��i�M{�&��N5�5�x����#pD��h���7���5���6����g��y�cy�y}~�	U��%qb8Jq#���OYi�~�t����uҨ"܋���:Zb��' �}�Ux � =�����7�����5)�K�eA��"(NxŇ��0՘�79�n٦,e`�0�!q��<ň;o���������~�Z��!Ȯ�Xev���F���g-CKg����Q�����AEs�0�Xz*sV��E�+33�ގ;�h_Մ_e��� a,�abt��	�`��,�ey��\F,4aw��խY@I�x5�~�DvF��7}�]cZE5u	�&�Z��XǮ��p�˻��3�E��O��X�_�Z�ѳ�h��&e�biX�oM�1�1ћbfn����Ԏ]���QY�tr���0��^e-��-������}���\���'6#kj�j�����<�m��:�$F�Hοd��������ћsY�|�͹"+]��7�[���{P�M4kG�J�j�����]��(B&v���t��*|�n߾��Z���5 �YKYU�p��z�^�q^F
k�	�`��Vu�j�ʷ�ˑ�%Y�hђ,�B��둺����U�I�>����]4%�v`���U�5"[u��C� Hj���f�Ǝ$n�H������hF���;�>Hj�C���xޑ�eG�O�4�GN�0����O��F+l'}��CC�0��}o]<i�}(޾g`��,`W��XT�)�^%`Ƙ��<4���U5��p|��N���G�`9�T�!ø��X������.s����A�.q���x�P bG��'Fů¥f0�B�F��G�.��&N��Bp[����@G��l�Zڠ�j������w�c���8nj_�UW�/���E{�^(Z��<����-�a�җU�X.���`��_���CM�B�0w���ey"f�Xq~|T,z,X�1�����3#�%���߉b�V�z'�&R������u��е��'hF�Ƽ5%�o�y�7�1�L�&j�=�]�0Z>g�M@���h=�GZ�(��������v�����E�b�&�&���'�9"3U���F鏇�f%�����We�ɿqőT�~Gv�r��z��hxC��=���9B��gލ�?=��OuN��W�.@����s���S�yd`ʞHR�@Ȇ��o<������/��iA��V���0M���Q���P�U�PW����.�1�yͯ�j��MՆ���?����$���~E�7`K�:m 0�s,�+j�:1����
�}�S�@B�O��>��q�#�@ۀ�аAF���O�bO�*O�2�FOE��q�M/�����z~��ʂD��Ox�=1w�����#�z����� ��}���mm���m@_cH��R-�5d��K��Հ.��4�w��?ہr�O4��-��c;�d�U�p©~��@��fɊC^/���PfleQ�L�g�xN�ݪ��]Y�v���{�x%q2�}�篮�˖%��v�FR�{���y�\|p���"�lEq{���z�61�?�Y��듩�ţ�ӭd|���v���W�d`�-���mD��C��5�oR��Af�A�ON3�b옅�1OΉY'V�$n���=�à
��#�Y�v�y����z4�뷞��z���`a{�y xV�``q̻^վ&L���O�O���X42�6В(�S�`>t(6��6�>�`��Ů���B{$H�n�:'����L��F"���7�G,|D��lѧ���@:R��4�#GZ��^>��|�<r&$����z&	�����@3���5[[#�[#U�HGp��o�S"ۃ����싇��d^LPmL��1�b�9X�.����HK�J4��2�@5�?����^�sq6�F��
g���r���~���m��F��y�߷���aD��zHܹ��Tk9���o�y����D��G&�#�����Fr�y�5&4�u�H�4�5��w�d��(��h>�2��f&V)_W�P�1M�-+�=ci*ޞx�����ԗ�`�4�o�Q��6�FA��٬��21�LFZ����LX�X�2�{)=�)3-CSWlK��ֿ[��P��GW����-Wf6�2�*���j��X��ZO쮻W����m	
*5�j�Av�m,���w��cѢ�49b���L��b_ּ͈,�Ԭ���D7�V�c�zeބ�CΉE�MxLq�1-+s&ƈ[��]���&@nI>�Ɏ��|��-��߽�����j�"��41|����@�mI�܏n�מ�b~��!lw��!`���Wrs2��˃�{���]��L�z�V�I
�ˉ� Y�`x���Dߩ�{�������������L�fAf� sU<������˭����" T�[Z���y��y>D�m�W9��8�?�̾��fg6�3��?2������<2�T���?�?�|x��y�	��wC�PGUб���=׽�������)����B�c~r<jX������|Fݟ�����<�KPf�V��+bD�ƨ�}��%\�t����ck0�:c�IK��Z��&�����C�?�"$γ���z����oo��k.h<b�#�,E�)�2Ĕ�d�sՒ96\D�n��Ħ��<� �]����A�?u���T���hb���% Ё�>�!.
7r�|���������6T����(�ci��$>G��ڋ��T3�ћ3r�ߙfɱ��!�`:j��ӥ����b��rM�T�H�I���V���Nb�,~~�)ǒ���v���D��+��$;ܖ��f�#{B�Я�� �%�4�=򐑃|�<����g�]I�/��:p�\���q����ǳȞ���/=w���A����X&���;�xr'��}��ђ�3mTb�9����/^�M���'��g�Z��������aS�aXE���>��~e�\ul�w���gIkm	���J �{�@���8�}����[T%=|�\!xY.�z.*pN4&�{�FY�qC,��/ �Vd����!��آT<�!��/c�V�z,t��Us0�(� B�>��ʨ�0��lQ.v]�M�`S�r\�2�j�K��K�ƥzơz֥*r��]�']��\��U���R��T����j��c�zkV�Naơ�#@�0ƵptO�ѵxT��O+мx�*I�.~#B�8@�(JJ8�D�G�E��JI��?��N�=Θ�=�8�<eT�n�?��U���s�	/�~qN���Nz���u�F�M�����x�O}FH�ūw�Nس�p;J0�� R���i�$���΀�_��A��{�(��K/)�����������Y���c�{
}c�N���[&L��AP0���~1{�W��T���S��6<Nt�Q�������寇�z&����J��1����Z0ȽsT�~$�`�?s�H��3�@HHI�j�.�k<��j��6��&N������;���4��ߦ�+����?�b"�K�LŔ3Gi�=�X��I,��X '����w򎀔�H�<�sQ\ �k��ΉF{��$�졿�����C�����CT���݃"[��.j����_��������`I��f���Y#��FM݈�|�;(:�o7[��/%A��o ��˴`����Z���i�!�5�g�9H���V��5� ��OM�z�=��_�C.���?;@�
��êxam�H%޳�e�v����C�L)|�Kl���E;f3�@�'�"˷Se_{ɞa���ͳ�_zU��o�]�/�/��]��l�o�s��T�{��r������x����d��m�ߕ����l�T���;�����TT`�+1��#��e��J�ki{_�jt�=��տ��VVL)\���_��2g"��10?�����?N��}�����4��H�Z[0���ߵ��i(�]�6����j4q�l�u��[� ��������o�{�����g:2Ʋc�)���>�д#q	P+�d80_X[�@����Zݪ�]@)�[�ID���3	orOC�^ vS�b��T�cah�.��m`l��Vѽ��`�����м���@��{�N�x��,o�ӱ�-�j�4�6�6Q�M����h�n��d��3�j�4~ց���R�C����Y�O@\k��E��Wn����b=�-"��Z�S���!�rW�SE*����@�	���f�J�C��Xx���j�	�P��
��m���5�c~Ƙ@!�c�.�z:@u���C�}��%�)�mC-|��^�ȍU�a���͸����L��3HB�b�x��U�-�jui(�]@�R���@<���Ydj������&�h�eE~>��#��RD�c������F-�#��7d(�E���`�*}�H�l� ��Z{���+?�2wxE��W(�~���R���^�	�ޫ;�
�n2��>V�mŒz��Hw��4� 6���2�G����)�!��
�B�:hw�*+WuL�F��$�ɷb�>����9����r�ۭT,�G�����c7/����Y���nl�8ڌ��Pu���:�$uVڟa�y�a�	$��:�[:4�ƫ�j��:_��uoi�� �]�W���˺�T�*��\�G�?�uo�@�x!�+`��7��n�D�U�c��{fL�k�T��q��@���� ����X��c�����=�&�f�j{��P����G41md�Ib�V�
o���%��Xf�e��v��r^�1R�j��&�:�U��o�mݦ�u����Z4G E&ց�_`e������H���Xu�0� f���\~Z`�6�͢yX�v�*VL���#�����Hx;�A#ׂx�H��A�A'b?�ގ3m��9�3��I��
d��A����+�D^�lg��:���S~F/�̔ܜ��3��oj�3��e�N�6���!͛��D@'�}r�O?�|Dk�'(���DF��#�~O��yQ��bW�g?��i���6��Up��w?{���a�n�"��+i��nU%��I��I�>���������}��ĆBG�U����N��:�sj@o����K5��4j ���o1��X��_[�-��p�О�����"!�v�X�?`��}�2�C
͊7t|�J�^�g��j�[��%�n��opD�]w�ou����틥����~nf�3O��	Z�M�j�ߙ	e�Ύɨ:0+۰����� �A�oRG��q.�*�x񩡟��/������p%r˕H �P��6{d۷��.�v�?�#X�y����3Me�%�fI}�S�+ٗm0�7���H��!
-D��-�6c,�y6i������7��k��JCzs��Lߴ�]=%v� ߙ��Id#>p_<��e4I�LJ4��0��C9�bg�$�Ծi@�:c�Um��`��@��*�$?�P3T�E9U�����X��� J�j|Rԛ1�
�AF�a$�X��3Թ��D��J~�f�B���c!�P���>@/\�ɑ=͇���'����3�PvI*�K����VQS��*�Dޠw�{(����R��\Ca�L�8�V!H�����3�Ѣ�����9&1�w���o�:�^�"�j���v�"F��ԓ!NA)���,l�,�ܨԨ�"qI�Ȋ=ֹ�5���KRq�$�|T�����hb#�B�Z
A6	DOC��G����EЂcl�Ľ͒M����E���|V��ԐQn6�� Ce�@F���)i!��#�.R�I#���߼�8�pc�z�#|�c�0.l��x��B[|b�w���"fp��Ӥ?{�Qdw�i�t�x^W(��>_OZX�	��\���<Q�=/�zֽ_���6Z��@�;����jZ��^���u�Ȳ�E�Ԥ���@�W��	o��+׽���R'źw![w]?��N�;1�J���d���s��Ll�� ��{��Љ�t� �)o�zX@��Q�Nu���-T��!ع�Ƽ����8~�� �r�Z�V��6��#��#XA*���3FL�E.�ɦ,Az1?�����Ļ�[-���������3w�8��(��)ƚ33tش�h�[�LK���Ã��S|�I�ј�'d1K�
3�b?;�W�Se�w��|=fI@�G���j߽���9���f�s�ʟ;���y�=�s�{����N�=?)Qz��[pr���	����$�fiFQ]��@ �vY�Bi�KU��������i����lRzK�6ޭU6�gp�/�m�ƅk��s�\�����9����\͹{�PL{nL��b�x�W�:=��$~��8��&�e���>`|o ��,�8����ʯ�6���y=���B�~]����qpU�Q����6��/��A 11jS6MB�4��w��+0K�p������0{F������ўo�Ưxg�dI�߽�^o��h�Hd���pX��@=���.&H+�^����nF��Ӂ���ժ�V=����� �l���p��ь��������ៜ��굶v[b���D�P?P�5	55���Pk��i�Z�U+�Bk��`3 XL���-�Z�Pc4�մ܎�ڡL~v��J�X�P�����b\�0(M��D�ּ舜|/�*R)xY#�zń2����r!�n��n]	9,��x�Fmp���ۓ�j>m-���O�T1܄+�6���=O? ��=�y����jV���|3�o���<��>\��J>��1�w��Qo;��[m� .=�͟��)C2xO�
)�iU�V���J���;yǌ@Hm�0S���J�ve�pW�ߎ�3�#�Nn����m��<.������f�jIK�8����A���#��8o�n�nB0�V��f�����I��b#f�l�p	���� �ۚ�&B��ՂH�j0��W �=N�C_�[�U��������?r��U�B[��}���T���!	�ݟ�y[�_I�=�>:;-4�zw��}��]h����@��W����o�l��*�f<jG}��0�n���B_���T�W��{�0�W>RÞ���ޜ�,G^
;��U�V���n�)~�6�7f)r�E����n)Zq �\�0c�Jg�\�)4(�?�RZ#�w��Yuh��$zp1٩Zk}}G=n���T>Y�i�Y%,V���I�۫�L_6������cjW�t���y&Yj���/��z�ZR�ߦ�0,�=!�E=�*hc��,$�R�U	&h۸�CY'%�2��C��&/ז�E�5�ʧ�w�R/��ʧQk����=�YU���O;5�[������B?���&2��@E��a�=��fp��6��A��<b�_ �xP�qm#"�kW�|���'-�L/����݆�V�JUb��i ��C�l�xk�L�%�7�[��֕�2 ~�u ����\8k�!x�%�����򺒿�ш�9�5�h�	�3\ն(�`��pնpjK1�Fm���8Ǜ�m��:����/խi�����N���Cn^�����L�wԵn��I5 �0�$�{��B����Y6�5 S�ϸ7l�!�Hl��������w�&��\��j⨶�$�Q$=$)�n"b���s�T�<��<�#j�śpO���:�-��$%b�$T+�j������iD��{�&V�b*J��H�o��*��E�m�0O�J����M��N ����w?��?*w�=��k���<���  � `�S)�ҭJ��=1!��&	�`��	TZ0�q �ho	8�� {$�=k���[$Ɨ����j�����Waa��<B�?؈�%�Ʉ��O��=s�K����Lل��0����!��7�N3$qipC5F#xY��Լo�QY���[v��G�T������Y��'�;�
��Vn\n�l=k��7(6=��8�W�r����wx|!@�q�G��ȯ�H�A��iuI���V&�	.��Ú�H|��C�ñ�C�����k��W�00?&�y����G�d7����Y���X��2��)��n�vCZ��S��U��ܻ�b�����SV�nܳ��zmN#�bÁ����/;T�I]�L����Mj�j/��F��Y�j�^�n\XR¹X�/��b��ͻZ۔G��R���1h���|�D:S6~�񅵊��/�9�����o�\�;Z��F�m ��m@�jD���ʗ�umC("U{Uy}^<~1�g%45c���W{S&RJ�2|��;*�H̽#��f<�>י0�	����Gl,�ֆ�o�`"" G>��Zڳ�w�"?2��C�+�]ї�]"�d l+�58���9G$��E�f��bI��^��I}Nw�,��e�/�wL�hɃ>i��/�)�����~Q�������^�{�uo
���O%t�r!\|��;y���=����i��?_%0'8OG��!@�r��,���̃`�^�XH�O�����N�<+��2�� �[����Y�{F	 ���O��M)�_�D���&ݸW�AdH�'�F�Ȅ�`�/ �C�|>�%:p�M�ʍ+`4ߞ����OI�&Vj��V3� Zv��X)	�2t�!	S����H�:pP�������L�����Ӏf�4��T㻪��6<�,��.��O$����_�@�9�E�G�	�k�ZKv~?q�1�y��������h?e�_Kn����ԯ�.?�0.��\��t[4�� ?9��.ͱ�&:,�n.�`�Ύ��y�D&57�Z�RaJL،���X��M�����{_8�S�O�Ѱ���$�a�#���۩w��P��f�ǋ���h��-_'qtѦ�	�9��F*qߩwc�4X��h��>�=�������b�;�/�[��o6�[������������͹{���zK�ߙlq�Q�m���ݲ�����Z@M�D�<��%�`g8s�� kSd�	����^tk�޴�|Q&��&�B]K`+*%X(�7���R�E��i2�La�2����9n:��J��I�	��:�\S¡L:d�O�:)�s/��3�8�sA��~�Oo�z����M~�G����=�e��V���h#O��Ț��:t��� Ɓ �3�7y�;0G�]�}�X�r�|S0G,;$�'.8.H?}J���O��w�p�)�t�^�+��E���� M*>��K���;^�����=�z1E�ǋi��&�J-�캃���i�ل5�Պ�g��o�����?���1��7nN<��
�}1��8+�E��aR2J�}���M�2'~���Q�Z�3#��	Q_^�5@��ўNsZ
���5@��������:6%�Cq�{�k��ɯeݼ��򹀶�f�\�@n�G���PZ�Z��C6�ؽ�(���M�hyކ�G�Ė��U�����^�f�B�<E����k�r:�7!_'W(V����>������Xr��z�\(}�d��P)k#F��/�l������9cx%����DQƞ�-�dBƞ��ǂ��ώ�������ǛH��ٻ����B����±�������T+�#��^�o��7|�ܑ�r(<�TxPwj��9�)^�/��K��]#��W���������d"\����O8΁G.�
x.<�l�[�ט=��?{���������9�#Hibf㑥�o30t�PU����d1}�s'a��/�y�����f顆��MO�6�x$����N�X^-y�h��X#}'��֠,~IA<�����\ݹѼ�g^���	pah!�<Ĥ#�W�ʺغ%�ޖW'��Rp2�ԠI��~3���r�4[�To~�ᔞ'�K��Զ��!����	>�X��i�;~�'�*ݓ���ÔUu��Gii�qȾ��,�5F���4�B�^LGk>X�5+�����3bg�պ/�I�]iFi$��%�9�I3�D����Tc�b:�a��~z��,�	weB�O��H/�j���u}��ZYشQ�K���@k�6+<څ*W>�n5�2���"6�Vs~Y��?�9��Kj��9���X��qR��<�%�h���(��؈�1��j���3�5g����b�>��(�q�Nn�`S@ǐt:E�1���t@�b�?+�sW��5���}��D�CC�("��(R����S'����N���nl��⪝SK���.�0��i��i���}N�{��a���Doa�3-���`��h���ZYꇝX��� ����I]�Ԇ�����tq�4L��N�p;�p��wůu� �z�(Oy�1@\���yD���b7��z�n�6.��̂����Dh�u��:����i���6/K�.L��i)��,"/�iQ��ׅwg��3T�n#BWV�}���y����*<=��Q4�b'������c:�pW�॑sV�Ap�NSm�Gf6�u��7#�[h��B,M]��0����N����Sg3�w�NA`wd�^b�;k�`��:�<��i2]̪m���e�	6�}�Ʃ���Z�����x�M�g�Tm����V�-T��8���p���v�)��v�E�ZJ�n�����H�O������!���$.�Á?]�8�zz�O%B-[�I7���On5R�e�j�V�0ɮ&b|��������$�g�r��mϘA(��V�H�?��#A����I.r��:ƶ�Xr�4���6���WMK"t���#o[�6?H���W�a|r�D��@S�e�4�O}'q"F���9�p;�Xr9��9���n���|��Pm�t�Y�9N�-Ņ�� ��b�c��]���L�-��l8��Ɯiu�wx��7�w�:��j���Z�	�5��� N���eo���Z����!�#H=a=oZ��)��u=�<raAe�VK��7��yD��V����=���^_����%��XA��ѧ��l��ٝs&�ZF_>s�����p5!#W�CW����V0DiPm�ϙbί�A��k�j��H�T%Af��P{��� X���� �WŅ�V5��j]�E�c~%��`}&�*�����)#�3m����������r���"F�w>G���[]�0�vy�z�4
��[��X�Tx�3t�����C���s��~�	�5^�`�G�(�B��޵�Ft!�X{|�YT�Ҥ�I�Ф�-�Ŷ!�)��(j�~�/A�D�-q�i��m����>�Q��TЌ����0�K��K�N�;�j��oS�^��65��zM<��&y���&���%Cl#��QZ�?r툘��t�˰��}��N`|}૴��NU�	�y��}��J� V �Y�'L��Uh���b�S|Y[=/񑢁��Z�T/ׯG���D0����#�-4S��ثNM��,�V};�$S�p�J.kŠ��Ө�0u7�?(Ɩ���4K��Y��/U+�#�����/����K��"uxb)_'i]�{
��� ��+�W����N[� R�����̫�L���(�A7p��Ϟ���"Ua.����7�{�%���c��� �?dU:=JdxG3�ӏ@��#xt�*���m��z�p�4Uǆ���W�[��"g�>��潄�l����F�r��\ߪz�N��n5m��[��?$ �T T�ԗ���m����*��W���4'P���	.�ȏ���b������'�"p�'�����ߺ��4�%�z���;IA@ߪ�,[�P{9T��]Rw%Lj��G�ͨ��Y�@���m�����цZ�0L��U�`�S�χ˯p��vMC&t�Dߞn�ck+�!{�����77!�3fx6<up����?��]��G�A���'7��.��{�9��3l���9��P��5M��S���K<��KѤ���MxE��+��<nD�{M�=��G7"St0��f(?��	G�Qs��`Q:<a���)@�ލT��q�mԮ���j�o�D�3x}�S�3�c�m$.���� � ��������O#�!=�����QP�#����M��=�z����xT��� �xli8�zZ��#��"����=0|��z��nQ2�Յ�^�t؛����S��t�:4�S-�P;,��{���N����J�{V'%�νw@���yL����4�w��a%�NvL-��ܙ����7 �崩������@�����J@� dw\��Ɲ K�W�z�
b{��1q2��}�V���B��C�LЕ�6 �z�38� �|��_�
D����I4\D��]�n$<:D���"&�%ynڷ$�n�÷�sfFs0��x�Z���K���-�au͕(�ȶ�t��i�������=���rg<�|�����`�_�w�p���A��^�3*Iᣳ�q&��#��C�ϲL��q�O'�s���oÿ�^r�?���0�xp��)#�P�2����1v����|�e=b�fMŘ��K0����g;	XN,P�M��G�14�"=fGJq3r,d*�5m#�Vb���y0R�)<`��1��K�[Qf(�k_i(��_Q$�s��ߠ������#t�@���-O�
O�S��k.�I�6�c�;s'�S�A�p�����iwڄ;��\�V2sŊ�]~�P��|&��{)�rUx�����_w�Ĝ=G��<�|8#q��F��{�C?@N�� ��m���ɵ��gjE���W�4�TǏ�y�z�4��sHR]��g��@K�*H�,�^�M��j�<���-� Uf����٧N���gD��Y6������8�J�@?:���5ږ��w�� 	���o XM��y�(��m>�W�n.�$�^yǁ��K����q��x:�����A?D���t ���0t[�]-�W}D�}�mѶ�6X�%	��#�E��]���ܚ\��:�������)��+��������I�@ϵ�� O'�srt�P��0�|�gH�O����ςod�@�?uIf;U��ۮ��?�G��F9`T��|ƮS�%�����6I�C��ǖ�1�8ʅ/��V�(�HR�X���@��)Z�,�t�s�I���t؁c��Z<cV5�9�U����a�= �N�>�dD��Ty�'��5{��z���Z�P�d6&�~9.��KGw|�-���S�uH�0�7 bd��!Pt��[5�[Gӎ'�y'�������:��U��>���"s<�)|�m��.ğƴ����P(�X�<��|U�h��$=qE���t� =*�Э�0դU׬4�d2���;[hd��v?'�����9�O�z��T�2���a�'��8�7�����?G�����֋���+��n�[Q_�Q����TJ�*$��_{�j���.i�A�m~������F]����� �\�vsv�s�% zK��dB�z M�ߠM���Q����I\�q;i��N	%!/p������h�ݠj�%��̨�g�h��ǃ����8��U�N��[_��M��]ڸb=���3��"St� �����x0E4gF�Ǧ%�[J��c�/�_ؚu8݅���:Cl�����}��~v��oM|�Pe������'��Z1.pH��-槥���#�=)��X��˿�ቪ�622��{��/��@Cw�jЕ#����	�a>��Myj�ڕ����Z�տ����
�Z�:l�G5�C��R�͂�����.�e�͕?�Y0��j���nSۤ�+ �Z �_�4Ӡ��H��x�'�c�c�9��%�n��K��s����S�j;�Zhn�x�?yp���8����S� �E7~<4`]��xa��{�д{qݿǑvJ-��]]h�h�49�:Hމ3~
�5 �[�&5#f~�q�OƻP���#q �/^L���@Yu�d#�gy"\����{��HX胿�[�ա8�'��w�}�e'z8뉶D��b���N;7�z��6Mr6&�.����.��>�Ԍ�����������E�������_4��<������󰭻�w��~�l\G�$@��O�q����>Ct��E���F��s��ZFp�xh��Q��.�xg|Ҷw�ŕ��a���Q����Vh{��-�o��,4�Dj�Y��Ր��T���tE�F!�$IO��2hk0�i|[�ZGo��7��y�Mq
E�YF*p�B�P����i���1�b�M�[�\���P����R�x�H��
�@�G�RH�i�
q:��\i�6�#͊hRa���j�ߌ#����0���8�"�ܷ��ǳ)�͊T� /}`m�@�| ����<�6�G�KѳkP��+VĦ+�;�W��::]��ⰅE���1@lV�)�t��k�\�Y�������0��8K^�J�b��x���U��\aW(f�f���Mc�f�Q�����?�auOτex�_�bւ9��/k��p[l���r��r����S�5�9��bˋc���n����X� ��O��J@{"Qͅ�B�L�߅���,"/�	W˚s�ln?��0,|f����gSn=7򻵤:^�)�B�W��1/�a~\wVܥ츫�L�[��<3]�pl�iۘ�ݘeޘS�Q��8K�G0;2�dqݳ�ls��d޸B��G�=�e��o�jcT.�ܒ*�۝�p&��L�,��u��U�����j�Ð��v�g60�N`����W|�h)�kυ��"����r,���R���Xl��c���cS�k�G����b����c���cc�k�G��Wq��ј核n~�Ex)0���R�����kR�mM��\��Ư��X����rcuak�뱻��������P����nF�@V �0n�&6$��2�[�G���L>�0ԯ%�Z�M���Q��9��#�U��(�Ǐ4~�tq�u�*���X�[��0o�% m��m=�}��ww��R����.\���ݿ�rJ��H?�
�\�U�~������%�������Hr����J��ʳ�+!ٱ͗b����RW�B��k�B�?Ǌo�_a`ZJ�%�X�c��xr�7����)��x������a�Q���E��g�o;��m�k_���4}su������v�>k�h*�� �g~q��F���q�z+A��G�=vd��2�(ӓ�2p⅀�?3��w��/�� R�P�������g�7m�ŭ�H�p�69[�~��%h�e���Ί7f�oOQ��p�|1gE���W�_\������\�-�����?St���p������;��{z��U��<"��|���6E��K��
�!{�Li�+]���TU>��>�e�ˋL�9.�X˽lK�86���ɕ�V�MF������l,�����1�m�*��GZ1��O��	�-�R���#��cAc���o��?b{潼_�ͅ�-[������U����?��ZKl��S�ø �Ā)rv�-�n�Z���w׻�����D��\�)�7�ln���Ņ����'�j1UaqڍٖOKN\��̦�LXP�!�-4Ob��D��L�* 1$�w���dӻ0}r����g8�Q���`����fƪ��L�}�Ԍ��Ӧb��!��^�aK�. ѵ�����3M��NO�����a��XH�`!V�>��:�:Շ����m�?���3;�'��5C�F�S�Iy�[h� �^tܟq�c�s�ж�p\�aX2t�*��������1Z�~�9�و��[|�cAVԧu<=����/���s1���:����3"��00����|����<ÙH�"FԳf�y8�|q(��\��z�z{�_|�
.B��.VMk��D�_����٪�`�>~�B82 �A��û�@�@�d5E��9�fa B�W>�s�?�.�sT]����?�*l����Ce����w �/{-Ud�s���#g��5��Of�m[�9u*??�қ�1�TU��K��ۑ0C�0'���~�Ug0{�|��M�K��/hLL��K��g�]��0q�%���v��W�F:��d�V������j�>�+PgC����W~�W�0Y�r��dr�)Nh)6��d:WS��a�^hO˞ќ=�ڎe+�1��T�xL0$�s0���h{�n�;�/���M?&cӻP�Z��}r�q*�]��{������ϡ4�|D���/b��F���#��IU��>����Na��LS���/��	J>�m^0kĂv=�JX���S�d���^��^^M�H�Jή{�3
���q>�)�T��v�ŀ�4'@
�`m�k�UJC������'FZ��\�����K�����)��X�&�pP���%'Y�x�U�l���0 f
���F�c��s<:yq|��J�(��K~=V���bU}��T'��Ŏq��DQ��.)<b�*޴(b���&�<{ϧ��HI�a��]�	���YS\1�x�k�(e�"F�?f���S&#/�4��۳�_��~+�w[b튋�Gv�^��ix�ˎs'��q�O�G����[���Ѫ��OD� >9���S#!4U���0��i��Q�nqx P/�.`쳨�"F:��W�th���[�7V�d����W}ǡ�M�rٍ�Yꀟf	�N_�pUѿb!��-0ŢV�9��
)��qﰀ��	R`��Θ��5'̞�
q��[@F�?b��O��?Չ2�B.�i����y�S�]K4�P�	�U�����:?W}�u��w�p��5��I<��{,m�?4�s����Z���9�� &o&X���j8�l��j_ǅnG7���8T6v�����F���m���۹z�F�I�s2FX�uKbl��f��x��i1��/V�`�
��h4�35+u��&�J�feK���` �j�}��$�&?�]�h^yؼ�vekB)�<��d�#z�+�kI��]�XѩмUHW@[��`��ѡm�vigR�{�L�}��N1�R_�� +����0|��C�f]Z�ۃ��H�Hm3���������V�n��)_'���*s�k��t�:����an>@x�>�z�3��]�*��Mmlm\�>����b�������<�Z��ce�fe��O �%�8�`l}?���ڛ�,���W����Iw^r$T�\u��I
|81�c�K���g
[�4:ti%q�E�PV�e�9�ϕ����`>��۸:/�V^�޽o>���^z�^�|h�'HC;��0	X��l�!�eP=t���#{���}�Ǹ�-S�O�nf�F�s�{if��X���ߞA���6J/M��)���{��{�7������<!f�o�^���7=�S�7݊�c��I�N�!*�}�{��,��=�@X�&��.�gSo"G�NtC�c��(v�3�S������1�X7������bU������/��D�h�ت�M���9��է-�[�� �19l��=��:�Q��+,h3C>1+͆P�@:4����{��2iEǅhj��ۂq���D�i�G30��U[`���&�#�G��NE0E8u�y�+eьi�7�Η�L�7/���n1�h���F_������HxGm$���B�����
'���}����Ƅ��v��H������b)O�I���y4��/֚��1�%�ך�F�貢6T���l���G��Iu31��*<�5wK?�7��]���Zڵ!k[d��w� %���,;��[t']L�lC��\��]�A�J� ��b�w^x�/^F��`\�=��o*�p�d<u��^�Ղ����yw����x��;�G^]<~�/C�x��~�F�r���^���ˉ�#�*�,C�^U��P�gr����rJ���c�������#?�~�Z�XkjKt���GVRBd������G�	V����׷�{�rƴ��N��x����am��膹��Ps�)��Xn���je`�_�?�KK_6)�w����bD�(0�F��$�t�*���_�]s�tQ�_ԡѥ���7�,ݢa�h�X��r}NK�ί���dÏ2��ۼ�t���P��wJ��Fs�7���,E�r���kT��� '���1��`����[��2)	���1x��6ƞ��9@@���E�Ѩҿ&�4��g�����Z����Z:�����Ұ�)�DGR�`w��1�.nt�X���p�u�l�4��3�����<�1��&!I��[�W�o)kY3�-R�;�s�0�γc�/��y�����\`.���d�� �o�^0�Y³II��%��,�K_ ���1���p�d��Y �Ȃ�
w��aJ��Fm�3C������Hs����l���1����4�^�p��i�~l_}y�z�'��>�k&�iƸn!J��W)D	-�����r���0��[HCB�N+j������ڛH�Q���ۨ�fR���=jB݂1��(�C+=�$.�$>�}m����t)�S���#�y���;��Q� Pv��L��.
��L�_D��J�Ekc>��%+d��,��LȖ�&���*BD-�`F�:b����(�;c�*/y�e,>p~�;nk�J�Q�m���bi
HÏ�7݅��r��n���`���I�,Am���:��~is�L%��B��Y��90��K{�mTKາ���ɽDVH���l$�Y:�b��(ߤ�����ӱ�|�QLո�⫙h����T����N�èy���y�TM��#-�������<�\w5�B���i�ߵ5��>��;xti��s�Z� ǫʫw�tK�}�9!������J���Q���c��Q�=՜0U��]Җ)��\c�$~�XS ��SF'�oY׆�;����#���s���=��Y�1iרL��rF�Q��#�殄TX���ƃw�7ԏ_���cҭ�f&�o��@���f��i[*�
����ܵ��5�- ��� x7)��mR����	G5n�M�h 'E���z2��m�t��w )�t)LE�)���  	6���[w��B������lSާ��4�ޕ�)dy�,�G&����~����.�x���G|����G�Ga���_��H�KJ����o*���L�?~�A�,@J��ݶ��u��]t[�Y��n��w�I���R�킥��E�ߥ��(���$���|�.����m���}��t���3�/w��#2�c�f[� C���3�b��^�s�3�w�6���6C�Ř��� ��&�l�WN�K}M�:�T�a��^�0�����.��`:�2�J��Tk̶��_���u��,t |��+���p�@:L#�-{:�1�l��C��(ڀn� �Q���Q��c!���]��Eğ���E�M��X�E�����#����/����N�c�J�7-�W(Z���'`)�AujS(���&��.�w�����l���9�L�-����J}���|1�C���T�2�g�z^�	n��3�^�dk����C"��!o�Uh�#���+�-/=ee�/����#�Pú���ETe@�f�����H"���e�T�`��������\D�K�t��pe;�!}�����6����ZP�Eo��d*v�iWڢ��/��&Ma�����]�Q�`�	��h[4��o���[�Q{ܛ�ݓsa
�);2�U�#��w�+��ZfN띢�]w1�_x���e�@���w�XP~%��J�z'�n:kd�	9�"���7'>p��>���#���^K��,�������gG�XE�]�Ok�0t��.���.�ꈠ~!�N��l�ǔY��-���}��j�j
�ϡ`j,��y�L,�}6�H�o�ي�D>mS�M~�"�e/�� ���)<�3s�D��7'���~ߣ>	��Z��粏Ъh	�s��EW�}{7E�6sL�M���� qF;_�q�Z��{D�ύV��٤!VC�������Nؖ5K��L{��O�������Sowi��є�E ���kƃ+q���}�����U�{-4��n����t̘3xȇl��:��<3fkg���3��ِt<<�N6e�$mkҌ�+?����>1�h����^3.^B}����}��B���?|��}��F�e��P�,���P�� 5-�e+~��tb��b��]Z�K�s� ����÷��u��3��_)������
D--8<��`�NA��(C#GDߛ7ٖ3r�y�d�J�x��	@��횪ֈ윺����-�`�����h0�G�߿��x��Aj+wh��	[ˑ�h�Ed��b�-�l������哚�g��J-� �h�w�z&~��{C�ot���R�g� �<�+��#�����V>p�ݹ����{W8�.�ko5�Vv�ab��wZ4m~ʷh�`�b��6���a���m�B���=H�_�V��f
"Ţӝ�X!�(��ۢ�VA��x> �� 2��c���!����T���&��9��n_�ψ�E�54�6`�Bd���x+5�R���1��O�o�����q���J��_��g�bݵ>+����L�m�� �l�-WJx�-�)���{'e��o����s�=��\��b���W�a�0O\9㻟<x{P)�)XQ���	��^X���"%e���7�"b�"����6���d�E��V������_�~�x�+��T��/$t����"p����$�ϒ,Os�\�I���U'���u���p���?�����zZ����!Ω:�%	ֱ��Fk3w �ٺ%܌�+Y�5z6Z�!��]�� �
=O�Iǐ�^�RD��D�9D���х�T�]�����dv2���= 1��Fh����v�.�5����΢���A������M���D	�Q&�64����[4��t��Q]��;�A];�*��R�p/.�+���Aҕ^�$�%�R�����vt��?���A*���?ـn`*�:DK��ZI�M"&%�4��fI���ש��U��"i����6I�V"iC�E�9	^[n�D�n��u�I;�5x
u�v��ȺU�[����Ϯo�&�0����`j��ω�ӝI<Q��pD�!�s�Џ�L�Ky�[�3�����Z4u�ň9.	o>4v�"~��Ue	��|��E����P�Z.���:�6(6	�qqZp���h��?DG�Ι�-;�rƗ�n+��W �AY��ȸ�6��9�vҢ%\�E��L�h|c�W+��L`�,^)_�+�I,��P���t��`]�[��t�ؒH`��Kv��cYg�)d�O8��Z��uW�����$�y`SWڎ�+���0b�}�1|�<7]��T�oc��O��r��Yp�"��$�������<��0�����i)�D{�]�[T��S<�t���~�Oø��n	qi�� g��`�q�b6f�5���N����.O��L|Xp���l.P٣��"����.�)'��+��	�G%��{����ʹ�:���WF>�K� �����r�e�f�Z��z�]���c�˵�����hM���)�Aô�S^c
_f0TR�e�5�;#��� �a�` p��d��� ؤ��cW(Xm)��۩�*�{b��w�;���R/:r����w�m��J�<!+5��ko` o;]���.ԮU���`�R����*�!NF�<���l��֒9��F�p�3x��͋qo�|���/��'�1b���F&�g?,Ю�c��A� M�Ч8���^��tq�tS�D��/�%�}�5�ǿ+�А��?{(#i��<�E0Gr�+���y�.�O���hpQ٭����D����������mf\d�b�Q�l�T��	S��*Ҟ��US^(�l� K��{�q��88�8��Lf��
������S��_w���T�`�\�'��Fd9�r�d���_z��ϰ�L�S������Z�����.	t=��M'�?�nF���
�^�:�Ü(���z�x�.����:�JԵ��K���O3�˸�^=d��;�������<\��p�i��tG����D���9��`�t˫��^Hu�����e��� �:3z��ț���J0�sB��"�巌3�!�$.��f�d|0�'�Z@S�6��3��aau�8p�#i.�]l:Ф�!0�Y>BŪ0л'%xtz}
�+,�D� u����vq�Xp��F�ْ����M�3���������� �zA׺ ]�����%1qRӳ&���I�6�`�V��X���� 	  ��[Rh�愰%S�Y��h/)���8уR�H �_@���m�Ȏ���)хGO�6��Ϡ`CG4��x��I�^�I���><�Rg?����{���\�t���)���X��D��+(��������*�g��z��C���M� �� 	4���`PXƩ���Н5�W%а��,[�t�d/XS���'-�$>0c��%	����ΈZqKG��k%ʷ�֨�����Q��=l�Ȼ���u-W-�t^��n\�c����i�G�XD�`0u�C�MWm1`��bMl���)��iïS��3`T��%��?$����!��� �"���P����O��i�T
Fmq�;$�A~mq}��6G\Q�n���ƅ��Ɨ��u�O��)p��#)��%#�����8�|MҬ:]��C���'�����2aɜ0��o��R�Ǧ�>ir�r,ԑ0՜:PK3��5U���5��KF��_�������e����u�o���p;�Ƶc>Gtݲ��3�v�]d+Y�k`�4� el�P�4���F�Y]�Ia	r+��(K�[D���R�.b�g�V{�y݋6х!�Q�ĸC"8��jc��"�oH��OZ�>?��q,j�1���#� $�wv��\\?펢��pEyD�f�U��������Ք�3��/&*��;ӉEB���X�Z\�+��n�M�O��)�>�.�M�I"��a�Z.	O�W~+�����-7ts���{��ߦbC�G��`�/��A6�(l}s��I���75傡/8w���T�j�����s�I�Y������D<:}��}o0��W��P��XǱ�;��O_����bؔ��͞rA�Z#	��D�Y"�ˤޯ�_rv`S���{���'�b��7�ٯb��
� �3)��VK�,ѡKʯ���r
?)��=��dy��\�1t�4�`R�?d
6qD�WR�$@��ʃϘ��&�����a���_�U^R"�2��t�$L��δkT����Z���Y�	t�;; ���P�K^Sz��������%	x�ur�Mt����Y���nyjpa�꾃��	��^{���g1 #��M#b;�N��M��v�%�n��裚��i�C���)�֛8�R���.x�c�$S�xёyR�z�%�B=�$�4�;$!>�S���r0;~.IQ)�y�<��v��͒H�&���` "�Z+XD:��V=g�5���yQĤ�R��s$�&�^)췕��P�O�G-C��狹��;-c��M�R4?h�!�E�����;d|��s~|�s��+=���q�������;���-�UHd9e W)�!'_�E/��l@�~��q�C`�.�N���۱e�7z閃�A�G<|ZXeh�6�jV�Z�#��;3LoH�����<�%�tTy�ty��O��gƟ4�du������;k�IW<����4��qwCP>����9M�-�a��K�XV͹I�4#X� �"؀�n��~�GE���q0�`�l�n�.]�G��	�n&a�^����j[huz���A������r��2U���5H����^;��%��:�{�Z�7��X�[Ҁ9~f�·�L�4�M��6(EL6�H�B����B�xp0��GQ���Nq��|�SEцb,|{L,�"�⣁cj�Įn��m�P¦&��72[\��
����ƚl�d^k��4<*����Ck?�����2h�/���U��{�U�H��yGS�9�@���h��Z�W��rb�.�<YO�n��DR_�'��Q��%K*3��;�Q�y��Nn�aٵ��^G��O%X������:�X�s��l�p~p	5�j'i��[h:2:������C�%P��/��AH�k����@ ������1ݷ�D6b��� �i��9l�%����x�S�Ӱ)�i��NG��'��t$�|بc$Z���	�����w;[��}W�U]X�I����_I�x9��řho��p�6c�uS��5z�������۟�r�ni�l�!w���ߨ�������g����z /��_CT\�a�¯�c"4�'`�kw�ӣצ�0Lx?���m������?;������ۚ8�D )��绛�ZH�	˫�/H���s�{��  M�$���g�+�M�I�n�
�!�D(0�:�*�a_��}r.6�'	�W�h��������|�=��Z"���'��̰��'Mţ���H�!�����j�֖��s�т%j��q�:w0�<�>5qZԉ����l�:I�a���D�dF��e�����H�kڨ��D��i�1�\ՑX�6Nв['>@1��YǶ�rꚜ�D���u�P����C Rϊ)H��H������}�����$�G<`�Ҕ}�=4����O�t�k�v��x��fy��|�yxm �)�e�G��-O�¶�_����n�c�&.�	�bO��Bz`+A��ߕ���f�����ro�G.=��@��߁�{���q�/��x"ǒn�D��Xp�x��Q�ޅbaDq=�+<AH'��]�m�3����m�UGCSY؊�u)gs�!^ ���l����X����,�4�w+�W�$u�o�hmC��2��C�n*I���}�(����?��D�.�%�,�NT�"@f<��1ƾ��������,]�bkE�D�Q��7��o`\�.�|YSwKGS�	��fs���7;;�X��׻{>�U ���P�}1���/�?��@�u}0�}�N�M�{zm	RA#J�@8AE�$����I�kAE����{�˟g�#�?PC0��GˁUI��Pٺ�1f��~�j49o��L��8��H|;��Dq��y���=nU8��������8��)�FR�tEj(jᣩ�~Z�=>��lsb�2A��]��>w��ɣO}(�g��5ȣ������rI�Q�T`�&�g³J!l�J������i�"s�'X�6��q4��wbiG�e�����X�pBQ�h��_����Xh,j��L��� Mb��g���a!�~BD!+��IiX�0��au��XD���g0l~/�[+t��\@lЇ׼_�Ŧ�X���S���aK�	I�,�	�>U��c5k� ?AwF�ܱ���ň�5	r,���n�j��@���z�n���K/��	3/�ۑTm�:��ot�Vsߌ�]�X��)�đ��}{\SǺ�ʋ���H@������ �[��-b���ح�&��XDh년��¦UBw�jCڴ+n9����]H_��[�Wh�٘mZ��m��̬�G�������s����e�H����o����f�!�9+���\'Q���Y8��Q�+�3R_�&��>#GB��~�ɜ���79=t˷�pc�2�r/�н�)��눮G�-A7�8���,��s�=�^Dt�(�ʦ�Kb7p\+�j$���!�U�E�d@x/%�uq�o���rĭ1�)����B�=�y�m���BP�K��m�F�B�&)�Ǣ�f�[�\�4)��e�;������va}�S,J���YZ���ӗl�����O���0-]%�g�����%ݱ(�#�3�b�KР��И��:�/U� �^1{qg�;�,=e��V�}?���9)����c�h�p���3����|e�N��`�1.޺��π_��H�\1���a��ǦZ<?d��������e��pdb�g*�p���V���E@�ŝ0�ρ��"X��l��Pi�[�js�ģ{���%p�S����4 �%K����]������<	��,�:����%��E�N�Nt�u�h�<�%Y2z�)r�$k�r+����fޟ�t�;ssR�QI�U<4�(�I����_[�3!?d0��\��Ý��G�k,aaZ��tfF�>eSI�`�����6����"���0d��H	�{k_���̂:��
S������lV	�H�q��q<�����[Ux�a.l�����65����i筡T�؛�Y%�|�@�c�� ���.����w�xx������^�_gﴟ�;F��
b-'I���K�����Ix��D�e�C"/���d��*��ܾ{�������_���OJO���~p��V݋u�x׊���t�*޾�!�'���ڽ�����b�.AJ�8�5j�VB������"��$Q?�D�M�s$�X!]�>w(�9�Ay.��Ʀ�N��4l�x���N�B���H��%R��4�f�2ǹ�"�N�m���ک�Ki����	�C�CC�z�6�	\��Խb|#�<������8�<�#�GQ_�A�{���N�*N�*�Ԋ�/y���b�D�Gzd<{���y��/�K2JK�Q���K�'�$0Oc$~ɸ���N#�� ��¹#�0 20K�,d��`��I�oDE�a`��U�9���'5���A�[{瓻�|��C�Wc�	 B��,�AC&S����ڀ�����m��C��	�����♡��C]��������!���D'(�N�s����q�xX;�"#aS���¿*�8�Jr�M�V��(4�?̲�v�(�
_���['�ΑE�gN�BН}#Fp�0ـ�A�k��#�]���"c0|�;��&�)���N;����1'�m�<�*֬���yIlO�<�������Czڹ��q	[�L���_����蟏&��r�{�#�F���F�-a�M��?%�P�}����
X��@�L�"ǯ\������`�*��2g�}3h�yGŹR^��@G7$
m�("]B�v=�̭Eы�v}
�rH$���F:���v�,�7���!�_x|1�%����H&%1�H�A��h_���/����E��;��7�~�h�aNB�s�r䣯�����N��i�M& �(�w�~{���=nm�:�L��q�=�N��d`S����,�H;�k�N�{��WQSa�#\���2�9�Wը��,�I���#�2����ա_�.��/�}�z�N����#0��Ɂ�zB,7f�t��c�n4d��mB+��p�#)�n"�mFC*�<���^� s��a�j:��Ш&%`	��5KDT��f�d�M"�?A�|p���P��ڷ_7q�s� �{0��p9�X�!vzg)3�L��4�D�Q�Q5��_ �D�w��)�)�"����ˁ����%�c�@�h_/ht�q�mĻ��*w�L�-�׾�1�!� o/t����d��׶��!gO���ڈ�y�3iX�,`�X	�7$EvF�Rx�/G��H�L ���?<{G{�A�֠1�����/3A+�?�?�<���-�0N��&=}��������d������%D��g2Ǖطp�1�D�� �
P&y0��U�;��y��Ѷa�.���7ھZO��O���8�|-����Y�؆.^'JJ *lt���$P����R�#��sn/��C.��n��q�,��|e"�%����	�1�Pj�(vP�q��{��g���jP����Az���+��.��	F�~�r��d^���	T&΍n>�6����S�D�P����g��Y���@W'���i��Y��,�5��.cl�f�awo�ACI}������d{sb�IlNX���J:�r+3OG��X��Ī~:��i~�{�Q�&Ɨ�����3�ڟ|�#��+���!!}-�km��][s�,"�)�T���g�Dɶ��vq�y�iT�02e|'��"�󶥧�ͤ��O�
��N#���e�Vb	~@?քL�;���}�L c�S$;\�A�c=�h��N�د�8G쳈���-1~"�*��d�C���Z�ډR�B�5��|��������'c���l&bg�s�c}EC=��-������C���}a��؋FO����O��	����ԣ��-��a���А&c`�^�C���תּ!�%o��(�-�3z=KO;o�7ږ	'WD��|�N��Ÿ���y���7}.-�9)T��7ƢI-���D5�0���}�M�8󓈦�I�\�
9Wg=LP=�ǯ������Z2�Ā��Pﱁ ,�\���EF��>��|��r�U��|?ӻ���!�F�;��!;�1�}p@���a:[�:'jv�2*s�C���cpW.v?)�&�8����/\�١�>�5cg({����?E��ìJG���]��û�*�&��{�'���l��w�Y~E��!�<� ��Iy�;�&��U�x��$�A�8��wyy����h�Ƴ����â��\��'�2w��y�ga���������j����m2m�[���aߑ�{6ӷf���+g�<���:	h�6ѐS��S�o�G�Ntu�� �f�p�P��,O���ކ�{�����#�&w�����Z�(_�'+�/(ǆȂ܂���_唯�)��)��)�E�h��|��ݹb㬀�ϲֿ/�����f� ���0��R�$�ʳ1~�#e�-�Օ�5���Nu�h�St�)-���=R��Q�Y��CòV4���vu�(��.����EtEd��u�̞��4��{R���}���0��X�7�$= Y�&	�]��@a/ڊbo���픸��1z51>�m�Ӗx��u�{mW��m�&���������kc�h��܃���5К1�v���r8)����t	��
�~hU���;�9&�L{��Q��ՂpE�����O�3��/V�G�5x4�9��o�WD�MT �O���&��&~J����J�UR4���V��Qџ�V�&N�&~N����8��K�S�n�1�������!f&]/wǌ�<3�ſ1Ҟc�����_�MT�&~L����@�W-b��X�T|)D9p(-R�+��Q��v	ޕ�0��]�a+e�J>M
�d�i_EU>��W>�7喌*�@[��^��x0�1J���E�����^��[RYl�(��������*VB�#�Y�,.�.̙����O����g�pz��{�J��ˤ��}��n1�_�g�~8���]N%�OQ�N]gt�&$V����:B�',A�A/��ے��&eVI���y`�<��<�s��,���q�	�WӕG�����l�4�`:ʇ3V�*�����fPT(�$�� -z�Y4��z�S����:K������wH�םw|B�t����I��u�N�ހ�h�,�&���� ̆).��e8���� ��y����tG�h f�p����0�����y� �����[2��t���9��砲��mP��1�����N�E��� +�+ � !h��P���	"��A?��� ϑy�Q��~�@=�;w&�E��F�n��}i[��m�7�E�9�u�ё~��-�~,��a~B��>��hD��44�'�g����㋾��/�,_�)w��q��|`��$FNJ�\��Q���)F%C4��|B*[�������h�͇�[��E]L��K�>�������*�����}YY�&	��p��j`[E4I���0�Āo~���4���Dz(�n�7��9��Yb��%N ~�$��I"4�����T�D�D>��r��L����ۥ�D�����,��
7������An���x⼁#y;�^Ѱ[mJ�o�hP\��UX:Jw������9|�ᯤ�O����3G��>�u�AI����9��q�>n቙���³c�7�����S������x�>�*�S:�z��!�δ������瘣[;�
{�j�
�x�7�&������ŷ�t�"
�"4������5����DpedX�O"�ֵ?��h����|��w��YE�������G��Ys�Aȡc����H���KY�G�gCO�I�����2Z��v���ᭊ�֪W)=X��l]M09$�g<��H����+���H^h�4ӳŐZ9&���x��r	V�YdHD uC"�(9lP��C5�a�����"�����tYs�Ч�P/!�L>�^�����i$o�:�Y�Lک�HH�æ�ဇ������@�����n�!y^h|i�0o�>�\u	HoJ���k�B�Q��k�c�軾��m�}� I�&&n(ghc�OJ]������f��0�T���{.I-�弊�"&��|C�.�A�Bݴ��.���~�ק�:�t/r��Ou��N���=1\�0�ܦ����6Ӻ�ݧ�_CZj�f��6uu�eo������m��{^?��3�[VDO��e������;�����-��!�&�^L�� vS�hrӶZ�i�����a���k��d��	�t,[�I������0�p�ąԷ1H�Cr��6]�f1���8��Ge��h�_��7w�v崳OF}���˄>�1T���P��˅�tl�s�[G��=?DYt�'�7��l�j�9.��Г��Y�6�,�@]q�'���2iC��7Pa��N8N>H�f�|�G��i�i�	�{.�0)8�Ю&�W�5����0�E�6@V4+�.�O}AlR��ZiB.�pU�O�����"2i'c�A��{@�9N������8��XU
��w7�3+m5�Qv�9`��G� d�A�8���o�b|)h����I+��l��"�(��_(�D�
PuA��:��Q����$��� �㒠�� W�^��Mx�a��٤@'φ�h:hχ��ba���������u�Ԅ@C��xPV:^֥�cH��D�p�c�B�Z)��$p�t8�t��mY�,�����4�I���F�2=���ft8��V� � ����V
s8���y�֢~'ā����;��<e��Ã�ڂ�ҷ�;� w>';���'�A�¾�DJp�_Jo���p:�I�����P�ս��\ml/�;q���0�����'�~&?JW���1���'��~���<�j�a����=�냻v��q����[#��z�~�lyߍ>�	���W9܀��Აڏ��s�@]/~''�ԵG�K�	%��g����owj�@+�>��Y�b�X�<*?i�f�r���-4�s�C� ����'��͸����i+M�ʁ#x;۸g������ҁG^��d͏�υ� [8i����`T�1�9��yt�`��>Gk�Jcǩs���(�
z·
P]V�7��+�7L[�<_�.~��6m��043�3]
���X��	�K���p�
@y���i�R�D o�'��Ep"�������
����C_V3��u>W�x���(o�{$)~��-{�b��1h�A�,�˧-/=���Z	".�=D{>�nP�L�g�k|IP�h�԰�d͂��<�]^�}������fhX͂gK�]eG_�b_�,��b�����L�U0QRG"�3ԥ��R�Uؖ  ���WID���&�T�@��%IS$.۾C�a3v7i�U����v����]�L��� ��i�/�@�"3����j����A�1�/�x���`�9�,x���Ȑ�w�C򢱄�w5�$u ��OZZCk=w��~��ٞ�h�g@Z�/q�XY�,yV�� ��-�`dAYT0)R�
z:�޵qd{��E\�P�5��/vN�Ӈ;M����,m)��蜾2Vk�i3=�E�В��9�E��Y�p�ʬ$����kVG����jn?�!�x_���E��B9v?�S����ӂ����0�>E�{�O7���7��@����Coq&{�L7���;F�uM7�@��K�rN���Ȗ�C-�S��dx9<x�J�g�0��"/�<���_O�M/�B�i��0���#���{Z,7!g>���uY%�L�����pI�c�R��2)� I|�V�/����W��1��Uy�"/o�6���B���fAf'����y��~�rqj�#��(�;�w��b�	�q�!�e7�D�P>~�����?<�v����-���[��\ﭩ��d���g�Y���zg $@�XBB�Pĵ� M�x�P[���E�{�T+H�+�BY��-��}ٹ��G��똠Q<�X�6����{ZИ2V[���
�L����XlY�3"��>+W~�Dt�g�kE���4d|�Mj����X*�j�)�Kf���~��DF��=)-��i(\�b&��oU^
��O;���G]+Z��Ӱ��ʡt�u���?h�y�%o�^��.-�_��3�����Gr�wS�K�ҭ���OӺ��s����:�B���\�R�C�b����l����U��J]�I铫���|��<T�c����f�*X=W�xy��c�Bl�2x���[>�/\[���_�����_�0�2PbY��*5j:TG�Am:��t�������W���!"�ѭ����q,|:G!Z�IS˅�o�������2�Q�H�G.�̸~u<l5�v��a 3�<:˼��yL�gf�ݘ^�t�m~jv��N�	�O�����)�j��Q%C�W��Va.�������!���d���{��KS锹��~k1�"��-Ój�� B�3-��@�x��%]���l�l5���dE�0����������n��r���
 z�)+�ڼ�ޢ��[s(�N?e?E��*��ls�X>�ev�l�Xߚ���K�ucm����5���s$���'���Ogr�3�Y9Y�,3u'7��Hp9�G̤�7��y�|�:¡����@���q�c�k~�� T�nh�HP��2�W8���}F�a׻@�F`�o�|?$���MU��~�|�q1�%���H��*�p�{��O_�u3�G������,�|=�1���d*
!r��-Q&�n�߻yʺ� pX	�[�~��_�5b5�u�����K�V�4�Rw����`���TeME]��"�����<Ҁ���4����_q�}i�"=�����?p/���|�8�?���#���R,F�jk�gx�Q�Q'���n���j��)�ś�0�\��7��j�%�{��1��u�;����+oДc5�X�xoMV'n�Iv����\L����^S[#NOI'@�\�4�A@��v��Z�,��m�� 	M��R�cM�����F\^[�{�FZ�x�����f�H
 �e�D��A�U 7�Uމ'V�\UC�����KQQܦ���H���O�P����$��JSZ����a{�j�@Y��b�n�P�J�ܥ�'���v�a ��2��NӠ�ّ,���lا�� �
M}C�F���.iͳ
}'���F,U��J��eIAI2��h��u�J7�U+7n.�/o*�nژW��`�F�[+Vn�".,ؘ�,ƀ�@6X��:�?`R�U@��`�]T��ߍ�k*5�\5;��v`���X](�x7VW����Yث Tvi�5�*��
���nؔ���������+W��&��v�W7����%r��Jl�8ai�fe��7?����|b2�y`tM���H��!_Y��#���9�m�+ݐ�b�i�=^�o.��8���d�KQkj�J�xI�ڵe���r7(K����s����̱*��G�7�m*NL���USu}W���_S�jwC)�v���э��0z^��Q�҂y�?\���@@Ak*�����?��9�����ښJ͎�Uuծ����P�w��ܗ����o�����������6��܂[pn�-���܂[pn�-���܂[pn�-�����7 �� � 