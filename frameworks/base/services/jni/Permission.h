#ifndef DALVIK_PERMISSION_H_
#define DALVIK_PERMISSION_H_

#define DROIDGUARD
#define DROIDGUARD_DEBUG

#define COMPAC

#include <sys/syscall.h>


/* COMPAC kernel structure
 *
 * The log TAG in dalvik is DALVIKHOOK
 *
 */


/* defined for syscall */
#define SETPERM 			188
#define GETPERM 			189
#define CONTROLPERM 		223

#define ADDPACKAGE			0
#define DELPACKAGE			1


#define INIT_PERMISSION_ID	100

#define PERM_LENGTH 30

#define PLENGTH 100
#define MAXPACKAGENUM		20
#define MAXPACKAGELEN		100

struct packagePermission
{
        char package_name[PLENGTH];
        long hash;
        struct xml_permission *x_permissions;
};



#endif

