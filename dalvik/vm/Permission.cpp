#include "Dalvik.h"
#include "Thread.h"
#include "Permission.h"
#include <sys/syscall.h>
#include <utils/Log.h>


extern struct packagePermission policyPackage[PERM_LENGTH];
extern int policyPackageCounter;

unsigned int hasPolicy = 0;
char policyPackageTemp[100];

/* COMPAC */

/*
 * ===========================================================================
 *      Utility functions{String handler}
 * ===========================================================================
 */

/* Used by policy Manager
 * Input Format: com.xxx.yyy
 */

static long computePkgHash(const char* str) {
	long hash = 0;
	const char *cp;
	const char *last = str;
	char c;
	if (str == NULL || *str == '\0')
		return 0;
	cp = str;
	while ((c = *cp++) != '\0') {
		if (c == '/')
			last = cp;
	}
	if(last == str) return 0;
	last--;

	cp = str;
	cp++;
	if(*cp == '\0') return 0;
	while (cp != last) {
		if (*cp == '/') {
			hash = hash * 31 + '.';
		} else {
			hash = hash * 31 + *cp;
		}
		cp++;
	}

	return hash;
}

static long computeStrHash(const char* str) {
	long hash = 0;
	const char *cp;
	char c;

	cp = str;
	while ((c = *cp++) != '\0') {
		hash = hash * 31 + c;
	}

	return hash;
}

/*
 * ===========================================================================
 *      system call wrpers, used by Jni.cpp and Thread.cpp
 * ===========================================================================
 */

int setperm(struct Thread* td) {

	int saved, current;
	saved = td->savedStackCounter;
	current = td->stackCounter;
	if (saved == current || td->stackFlag == 0) {
		/* Definitely it works, can NOT be remove in order to filter out package */
		return 0;
	} else {

		td->savedStackCounter = current;
		td->stackFlag = 0;
		if (saved < current) {
#ifdef COMPAC_DEBUG
			ALOGE("DALVIKHOOK ADD packageName=%s", td->stackTop->packageName);
#endif
			return syscall(SETPERM, td->stackTop->packageName, ADDPACKAGE);

		} else {
#ifdef COMPAC_DEBUG
			ALOGE("DALVIKHOOK DEL packageName=%s", td->savedPackageName);
#endif
			return syscall(SETPERM, td->savedPackageName, DELPACKAGE);

		}
	}

	return 0;

}

int policyPackageInit() {

	if (hasPolicy < getuid()) {

		memset(policyPackage, '\0',
				PERM_LENGTH * sizeof(struct packagePermission));

		policyPackageCounter = syscall(CONTROLPERM, getuid(), NULL, 0,
				policyPackage, PERM_LENGTH * sizeof(struct packagePermission),
				5);

		for (int i = 0; i <= policyPackageCounter; i++) {
			policyPackage[i].hash = computeStrHash(
					policyPackage[i].package_name);
		}

		hasPolicy = getuid();

		ALOGE("COMPAC uid(%d)-policy package counter: %d", getuid(), policyPackageCounter);

		return 0;
	}

	return -1;
}

int threadPermInit(struct Thread* newThread, struct Thread* oldThread) {

	newThread->stackCounter = 0;
	newThread->savedStackCounter = 0;
	newThread->stackFlag = 0;

	if (oldThread == NULL) {
		newThread->stackTop = NULL;
	} else {
		if (newThread->stackTop == NULL && oldThread->stackTop != NULL) {

			ALOGE("DALVIKHOOK forking new thread");
			Permission *temp = oldThread->stackTop;
			while(temp != NULL ){
				syscall(SETPERM, temp->packageName, ADDPACKAGE);
				temp = temp->pre;
			}
		}
	}

	return 0;
}

/*
 * ===========================================================================
 *      Package/Permission Stack functions and helpers
 * ===========================================================================
 */


static void permPush(struct Thread* td, const Method* method, char* pkg) {

	struct Permission* stackTop = td->stackTop;
	if ((stackTop != NULL)) {
		if (stackTop->packageName == pkg) {
				stackTop->counter++;
#ifdef COMPAC_DEBUG1
			ALOGE("DALVIKHOOK [%d][%d][%d] CAL method=%s| class=%s pkg=%s",
					td->stackCounter, stackTop->counter, td->systemTid,
					stackTop->method->name, stackTop->method->clazz->descriptor,
					stackTop->packageName);
#endif
				return;
		}
	}
	// COMPAC and thread copy
	td->stackCounter++;
	td->stackFlag = 1;
	td->savedPackageName = NULL;

	struct Permission* temp = (struct Permission *) malloc(
			sizeof(struct Permission));
	temp->packageName = pkg;
	temp->counter = 0;
	temp->method = method;
	temp->pre = stackTop;

	td->stackTop = temp;
	stackTop = temp;

#ifdef COMPAC_DEBUG
	if (stackTop != NULL) {
		ALOGE("DALVIKHOOK [%d][%d][%d] CAL method=%s| class=%s pkg=%s",
				td->stackCounter, stackTop->counter, td->systemTid,
				stackTop->method->name, stackTop->method->clazz->descriptor,
				stackTop->packageName);
	} else {
		ALOGE("DALVIKHOOK TOP = NULL");
	}
#endif

	setperm(td);
}

static void permPop(struct Thread* td, const Method* method) {
	struct Permission* stackTop = td->stackTop;
	if (stackTop->counter == 0) {

		if (stackTop != NULL) {
#ifdef COMPAC_DEBUG
			ALOGE("DALVIKHOOK [%d][%d][%d] RET method=%s| class=%s pkg=%s",
					td->stackCounter, stackTop->counter, td->systemTid,
					stackTop->method->name, stackTop->method->clazz->descriptor,
					stackTop->packageName);
#endif
		} else {
		 // Stack Underflow
			ALOGE("DALViKHOOK TOP = NULL");
			return;
		}

		struct Permission* temp;
		td->stackCounter--;

		temp = stackTop;
		td->stackTop = stackTop->pre;

		td->savedPackageName = temp->packageName;
		td->stackFlag = 1;

		free(temp); //free popped package node

		setperm(td);

	} else {
			stackTop->counter--;
#ifdef COMPAC_DEBUG1
		ALOGE("DALVIKHOOK [%d][%d][%d] RET method=%s| class=%s pkg=%s",
				td->stackCounter, stackTop->counter, td->systemTid,
				stackTop->method->name, stackTop->method->clazz->descriptor,
				stackTop->packageName);
#endif
	}
}


//-----------------------------------------------------------------------------------------

static char* policy(const Method* method) {

	// white list
	if(method->clazz == NULL) return NULL;
	if(method->clazz->descriptor == NULL) return NULL;
//	strcpy(policyPackageTemp, method->clazz->descriptor);
//	ALOGE("DALVIKHOOK %s", policyPackageTemp);
	long hash = computePkgHash(method->clazz->descriptor);

//	long hash = computePkgHash(policyPackageTemp);
//	hash++;

	for (int i = 0; i <= policyPackageCounter; i++) {
		if (hash == policyPackage[i].hash) {
#ifdef COMPAC_DEBUG1
			ALOGE("DALVIKHOOK counter=%d %d %s", i, policyPackageCounter,
					policyPackage[i].package_name);
#endif
			return policyPackage[i].package_name;
		}
	}

	return NULL;
}

void method_hook_call(Thread* self, const Method* method,
		const Method* oldmethod, const char* hookname) {
	char* temp;
	if (method == NULL) return;

	if ((temp = policy(method)) != NULL) {
		permPush(self, method, temp);
	}

}

void method_hook_return(Thread* self, const Method* methodtoreturn,
		const Method* gotomethod, const char* hookname) {
	if (methodtoreturn == NULL)  return;

	if (policy(methodtoreturn)) {
		permPop(self, methodtoreturn);

	}
}

/* 0: fail 1: succeed */
/*
 static int substring(const char* str1, const char* str2) {
 const char *l_s = str1;
 const char *s_s = str2;
 do {
 if ((*l_s) != (*s_s))
 return 0;
 l_s++;
 s_s++;
 } while (((*s_s) != '\0') && ((*l_s) != '\0'));
 return 1;
 }
 */
/* equal = 1; NOT equal = 0 */
/*
 static int superStrCmp(const char* lpkg, char* pkg) {

 const char* descriptor;
 char* policyPackage;
 char c, d;

 descriptor = lpkg;
 policyPackage = pkg;
 if (descriptor == NULL || policyPackage == NULL)
 return 0;

 descriptor++;

 while ((c = *policyPackage++) != '\0') {
 d = *descriptor++;
 printf("%c", d);
 if (d == '\0')
 return 0;

 if (c == '.') {
 if (d != '/')
 return 0;
 } else {
 if (c != d)
 return 0;
 }

 }
 printf("%c", *descriptor);
 if (*descriptor++ != '/')
 return 0;
 while ((d = *descriptor++) != '\0') {
 printf("%c", d);
 if (d == '/')
 return 0;
 }
 return 1;
 }
 */
/* the filter added in Dalvik for developer test purpose */
/*
 static inline void hardcodePolicy(Thread* self, const Method* method) {
 if (substring(method->clazz->descriptor, "Lzcx/webview2/web2")) {
 dvmThrowRuntimeException("webview2 is disabled");
 return;
 }
 //do something here
 return;
 }
 */

/*
 static void permTraverse(struct Permission** top)
 {
 //	printf("\nStack Info:");
 struct Permission* temp;
 temp=*top;
 while(temp!=NULL)
 {
 //	printf("\ntraverse id=%d",temp->id);
 temp=temp->pre;
 }
 //	printf("\nEnd of traverse\n");
 }
 */

/*
 static inline bool policy(const Method* method) {
 // Ljava | Llibcore | Landroid | Lorg | Lcom/android | Ldalvik | Lsun | Lcom/google/android
 const char* a = "Ljava";
 const char* b = "Llibcore";
 const char* c = "Landroid";
 const char* d = "Lorg";
 const char* e = "Lcom/android";
 const char* f = "Ldalvik";
 const char* g = "Lsun";
 const char* h = "Lcom/google/android";
 const char* i = "Ljunit";

 // black list
 if (!(((substring(method->clazz->descriptor, a))
 || (substring(method->clazz->descriptor, b))
 || (substring(method->clazz->descriptor, c))
 || (substring(method->clazz->descriptor, d))
 || (substring(method->clazz->descriptor, e))
 || (substring(method->clazz->descriptor, f))
 || (substring(method->clazz->descriptor, g))
 || (substring(method->clazz->descriptor, h))
 || (substring(method->clazz->descriptor, i))))) {
 return true;
 }

 // white list

 if (!strcmp(method->clazz->descriptor, "loadUrl")) {
 return true;
 }

 return false;
 }
 */
